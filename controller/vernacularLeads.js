const { demographicService } = require('@services');

const getVernacularDivisionLeadData = async (leadScoreList) => {
  let locationMapLeadScoreList = [],
    leadVernacularDivision = {};

  locationMapLeadScoreList = await demographicService.mapLocationDataToLeadScoreList(leadScoreList);
  locationMapLeadScoreList.forEach(({ location }, index) => {
    if (!leadVernacularDivision[location || 'Default']) {
      leadVernacularDivision[location || 'Default'] = [];
    }

    leadVernacularDivision[location || 'Default'].push(locationMapLeadScoreList[index]);
  });

  return leadVernacularDivision;
}

module.exports = {
  getVernacularDivisionLeadData
}