const { ameyoService, leadService } = require('@services'),
  { logHelper } = require('@helpers'),
  bugsnag = require('@bugsnag');

const getFollowUpLeads = async () => {
  let dispositionPatientList = [],
    followUpLeadsList = [];

  try {
    dispositionPatientList = await ameyoService.getDispositionPatientList();
    dispositionPatientList = await leadService.removeExceptionPatientId(dispositionPatientList);
    followUpLeadsList = await leadService.getCampaignLeadScoreList(dispositionPatientList);
  } catch (err) {
    logHelper.log(err);
    bugsnag.notify(err);

    followUpLeadsList = [];
  }

  return followUpLeadsList;
}

const getCallNotConnectedLeads = async () => {
  let callNotConnectedPatientList = [],
    notConnectedCallLeads = [];

  try {
    callNotConnectedPatientList = await ameyoService.getCallNotConnectedPatientList();
    callNotConnectedPatientList = await leadService.removeExceptionPatientId(callNotConnectedPatientList);
    notConnectedCallLeads = await leadService.getCampaignLeadScoreList(callNotConnectedPatientList);
  } catch (err) {
    logHelper.log(err);
    bugsnag.notify(err);

    notConnectedCallLeads = [];
  }

  return notConnectedCallLeads;
}

const getShortDurationConnectedLeads = async () => {
  let shortDurationConnectedPatientList = [],
    shortDurationConnectedLeads = [];

  try {
    shortDurationConnectedPatientList = await ameyoService.getShortDurationCallConnectedPatientList();
    shortDurationConnectedPatientList = await leadService.removeExceptionPatientId(shortDurationConnectedPatientList);
    shortDurationConnectedLeads = await leadService.getCampaignLeadScoreList(shortDurationConnectedPatientList);
  } catch (err) {
    logHelper.log(err);
    bugsnag.notify(err);

    shortDurationConnectedLeads = [];
  }

  return shortDurationConnectedLeads;
}

module.exports = {
  getFollowUpLeads,
  getCallNotConnectedLeads,
  getShortDurationConnectedLeads
}