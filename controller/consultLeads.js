const _ = require('lodash'),
  Promise = require('bluebird');

const { consultService, leadService } = require('@services'),
  { logHelper } = require('@helpers'),
  bugsnag = require('@bugsnag');

const getAssistUnpaidConsultationLeads = async () => {
  let unPaidConsultPatientList = [],
    nonExceptionConsultPatientList = [],
    assistConsultLeadList = [];

  try {
    unPaidConsultPatientList = await consultService.getUnpaidConsultPatientList();
    nonExceptionConsultPatientList = await leadService.removeExceptionPatientId(unPaidConsultPatientList);
    assistConsultLeadList = await leadService.getCampaignLeadScoreList(nonExceptionConsultPatientList);
  } catch (err) {
    logHelper.log(err);
    bugsnag.notify(err);

    assistConsultLeadList = [];
  }

  return assistConsultLeadList;
}

const getAssistPaidConsultationLeads = async () => {
  let consultPatientData = {},
    summarizedConsultPatientList = [],
    paidConsultPatientList = [],
    nonDoctorConsultationPatientList = [],
    entireConsultPatientList = [],
    assistConsultLeadList = [];

  try {
    consultPatientData = await consultService.getPaidConsultationPatientData();

    summarizedConsultPatientList = _.get(consultPatientData, `summarized`, []);
    paidConsultPatientList = _.get(consultPatientData, `paid`, []);
    nonDoctorConsultationPatientList = _.get(consultPatientData, `nonDoctorConsult`, []);

    [ summarizedConsultPatientList, paidConsultPatientList, nonDoctorConsultationPatientList ] = await Promise.all([
      leadService.removeExceptionPatientId(summarizedConsultPatientList),
      leadService.removeExceptionPatientId(paidConsultPatientList),
      leadService.removeExceptionPatientId(nonDoctorConsultationPatientList)
    ]);

    entireConsultPatientList = _.union(summarizedConsultPatientList, paidConsultPatientList,
      nonDoctorConsultationPatientList);

    assistConsultLeadList = await leadService.getCampaignLeadScoreList(entireConsultPatientList);
  } catch (err) {
    logHelper.log(err);
    bugsnag.notify(err);

    assistConsultLeadList = [];
  }

  return assistConsultLeadList;
}

const getAssistRecentPaidLeads = async () => {
  let consultPatientData = {},
    recentPaidPatientList = [],
    assistConsultLeadList = [];

  try {
    consultPatientData = await consultService.fetchLeadDataByRecency();
    recentPaidPatientList = _.get(consultPatientData, `recent`, []);

    recentPaidPatientList = await leadService.removeExceptionPatientId(recentPaidPatientList);
    assistConsultLeadList = await leadService.getCampaignLeadScoreList(recentPaidPatientList);

  } catch (err) {
    logHelper.log(err);
    bugsnag.notify(err);

    assistConsultLeadList = [];
  }

  return assistConsultLeadList;
}

const getAssistPastPaidLeads = async () => {
  let consultPatientData = {},
    pastPaidPatientList = [],
    assistConsultLeadList = [];

  try {
    consultPatientData = await consultService.fetchLeadDataByRecency();
    pastPaidPatientList = _.get(consultPatientData, `nonRecent`, []);

    pastPaidPatientList = await leadService.removeExceptionPatientId(pastPaidPatientList);
    assistConsultLeadList = await leadService.getCampaignLeadScoreList(pastPaidPatientList);

  } catch (err) {
    logHelper.log(err);
    bugsnag.notify(err);

    assistConsultLeadList = [];
  }

  return assistConsultLeadList;
}

module.exports = {
  getAssistPaidConsultationLeads,
  getAssistPastPaidLeads,
  getAssistRecentPaidLeads,
  getAssistUnpaidConsultationLeads
}