module.exports = {
  consultLeadController: require('./consultLeads'),
  followUpLeadController: require('./followUpLeads'),
  vernacularLeadConstroller: require('./vernacularLeads'),
  paymentLeadController: require('./paymentLeadController')
}