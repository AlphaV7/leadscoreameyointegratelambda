const { leadService, orderService } = require('@services'),
  { logHelper } = require('@helpers'),
  bugsnag = require('@bugsnag');

const getFailedPaymentLeads = async () => {
  let failedPaymentPatientList = [],
    failedPaymentPatientScoreList = [];

  try {
    failedPaymentPatientList = await orderService.getFailedPaymentPatientList();
    failedPaymentPatientList = await leadService.removeExceptionPatientId(failedPaymentPatientList);
    failedPaymentPatientScoreList = await leadService.getCampaignLeadScoreList(failedPaymentPatientList);
  } catch (err) {
    logHelper.log(err);
    bugsnag.notify(err);

    failedPaymentPatientScoreList = [];
  }

  return failedPaymentPatientScoreList;
}

module.exports = {
  getFailedPaymentLeads
}