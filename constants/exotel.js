module.exports = {
  EXOTEL_API_LIMIT: 200,
  EXOTEL_API_DELAY: 5000,
  EXOTEL_API: {
    location: `https://docsapp1:19d9daa95fcd2c59277a704727cc0b2361e27297@twilix.exotel.in/v1/Accounts/docsapp1/Numbers/s` // eslint-disable-line max-len
  },
  EXOTEL_XML_RESPONSE_PATH: {
    state: 'TwilioResponse.Numbers.0.CircleName.0'
  }
}