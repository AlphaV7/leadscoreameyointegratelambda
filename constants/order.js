module.exports = {
  NON_PAID_USER_ATTRIBUTES: ['patientId', 'couponCode'],
  REQ_ORDER_ATTRIBUTES: ['patientId', 'couponCode'],
  GOLD_PACKAGE_ID: {
    annual: ['5002', '5006'],
    starter: ['5003']
  },
  ORDER_PAYMENT_SUCCESS_STATUS: ['success'],
  ORDER_PACKAGE_NAME_LIST: ['DocsApp Gold - 999', 'DocsApp Gold - 100', 'DocsApp Gold - 599',
    'डॉक्सऐप गोल्ड ', 'DocsApp Gold', 'DocsApp Gold - 699', 'DocsApp Gold - 799', 'Consult Package 999',
    'DocsApp Gold - 849', 'Consult Package 899', 'Premium Care Unlimited - 999'],
  ORDER_GOLD_PACKAGE_TYPE: ['health', 'consultPackage', 'healthPackage', 'consultRating'],
  ORDER_PAYMENT_TYPE: {
    payLaterConsultation: 'PAYLATERCONSULTATION'
  },
  ACTIVE_SUBSCRIPTION_QUERY: {
    v1: `
          select s1.*
          from Subscription s1 
            left join Subscription s2
            on s1.patientId = s2.patientId
              AND s1.status = s2.status
              AND s1.createdAt < s2.createdAt
          where s2.id is null
            and s1.patientId in (:patientIdList)
            and s1.status = true
            and s1.packageId in (
              select packageId
              from package
              where tag like '%gold%'
            )
        `
  }
}