module.exports = {
  REQ_DISPOSITION_CODE: ['Hot lead', 'May be', 'Moderate'],
  REQ_CALL_TYPE: ['auto.dial.customer'],
  REQ_CAMPAIN_ID: [7, 16, 24],
  REQ_DISPOSITION_ATTRIBUTES: ['phone', 'dispositionCode'],
  CALL_CONNECTED_SYSTEM_DISPOSITION: ['CONNECTED'],
  CALL_CONNECTED_CALL_TYPE: ['auto.dial.customer'],
  CALL_NOT_CONNECTED_SYSTEM_DISPOSITION: ['NO_ANSWER', 'CALL_HANGUP', 'FAILED', 'CALL_NOT_PICKED', 'NUMBER_FAILURE',
    'PROVIDER_TEMP_FAILURE', 'BUSY', 'CALL_DROP', 'ATTEMPT_FAILED', 'PROVIDER_FAILURE', 'NUMBER_TEMP_FAILURE'],
  CALL_NOT_CONNECTED_CALL_TYPE: ['auto.dial.customer'],
  PACKAGE_SALE_CAMPAIGN_ID: [20, 17, 24, 45, 46],
  FOLLOW_UP_CAMPAIGN_ID: [17],
  FOLLOW_UP_QUERY: {
    v2: ` select distinct o.phone
          from customerSupportCallDetails as o
          inner join (
            select phone, max(createdAt) as maxDate
            from customerSupportCallDetails as t
            where systemDisposition = 'CONNECTED'
              and campaignId in (17, 24)
              and createdAt > NOW() - INTERVAL 32 DAY
              and phone not in (
                select distinct phone
                from customerSupportCallDetails
                where campaignId in (17, 24)
                  and systemDisposition = 'CONNECTED'
                  and dispositionCode in ('Sold', 'NIT', 'Not happy with service',
                  'Support call', 'Dont call again', 'Refund call')
                  and createdAt > NOW() - INTERVAL 31 DAY
              )
              and phone in (
                select distinct phone
                from customerSupportCallDetails
                where campaignId in (24)
                  and systemDisposition = 'CONNECTED'
                  and dispositionCode not in ('Sold', 'NIT', 'Not happy with service',
                  'Support call', 'Dont call again', 'Refund call')
                  and (
                    (createdAt > NOW() - INTERVAL 5 DAY and createdAt < NOW() - INTERVAL 4 DAY)
                    or (createdAt > NOW() - INTERVAL 11 DAY and createdAt < NOW() - INTERVAL 10 DAY)
                    or (createdAt > NOW() - INTERVAL 18 DAY and createdAt < NOW() - INTERVAL 17 DAY)
                    or (createdAt > NOW() - INTERVAL 26 DAY and createdAt < NOW() - INTERVAL 25 DAY)
                    or (createdAt > NOW() - INTERVAL 31 DAY and createdAt < NOW() - INTERVAL 30 DAY)
                  )
              )
            group by phone
          ) i on o.phone = i.phone and o.createdAt = i.maxDate
          where campaignId in (20, 24)
            and o.systemDisposition = 'CONNECTED'
            and o.dispositionCode not in ('Sold', 'NIT', 'Not happy with service', 'Support call',
            'Dont call again', 'Refund call')
            and o.createdAt < NOW() - INTERVAL 3 DAY`,
    v3: ` SELECT DISTINCT o.phone
          FROM customerSupportCallDetails AS o
          INNER JOIN (
            SELECT phone, max(createdAt) AS maxDate
              FROM customerSupportCallDetails AS t
              WHERE systemDisposition = 'CONNECTED'
                AND campaignId IN (17, 24)
                AND createdAt > NOW() - INTERVAL 32 DAY
                AND phone NOT IN(
                  SELECT DISTINCT phone
                  FROM customerSupportCallDetails
                  WHERE campaignId IN (17, 24)
                    AND systemDisposition = 'CONNECTED'
                    AND dispositionCode IN ('Sold', 'NIT', 'Not happy with service', 'Support call', 
                      'Dont call again', 'Refund call')
                    AND phone IN (
                      SELECT DISTINCT phone
                      FROM customerSupportCallDetails
                      WHERE campaignId IN (24)
                        AND systemDisposition = 'CONNECTED'
                        AND dispositionCode NOT IN ('Sold', 'NIT', 'Not happy with service', 'Support call', 
                          'Dont call again', 'Refund call')
                        AND (
                          (createdAt > NOW() - INTERVAL 5 DAY AND createdAt < NOW() - INTERVAL 4 DAY)
                          OR (createdAt > NOW() - INTERVAL 11 DAY AND createdAt < NOW() - INTERVAL 10 DAY)
                          OR (createdAt > NOW() - INTERVAL 18 DAY AND createdAt < NOW() - INTERVAL 17 DAY)
                          OR (createdAt > NOW() - INTERVAL 26 DAY AND createdAt < NOW() - INTERVAL 25 DAY)
                          OR (createdAt > NOW() - INTERVAL 31 DAY AND createdAt < NOW() - INTERVAL 30 DAY)
                        )
                    )
                    AND createdAt > NOW() - INTERVAL 31 DAY
                )
                AND phone IN (
                  SELECT DISTINCT phone
                  FROM customerSupportCallDetails
                  WHERE campaignId IN (24)
                    AND systemDisposition = 'CONNECTED'
                    AND dispositionCode NOT IN ('Sold', 'NIT', 'Not happy with service', 'Support call',
                      'Dont call again', 'Refund call')
                    AND (
                      (createdAt > NOW() - INTERVAL 5 DAY AND createdAt < NOW() - INTERVAL 4 DAY)
                      OR (createdAt > NOW() - INTERVAL 11 DAY AND createdAt < NOW() - INTERVAL 10 DAY)
                      OR (createdAt > NOW() - INTERVAL 18 DAY AND createdAt < NOW() - INTERVAL 17 DAY)
                      OR (createdAt > NOW() - INTERVAL 26 DAY AND createdAt < NOW() - INTERVAL 25 DAY)
                      OR (createdAt > NOW() - INTERVAL 31 DAY AND createdAt < NOW() - INTERVAL 30 DAY)
                    )
                )
            GROUP BY phone
          ) i ON o.phone = i.phone
          AND o.createdAt = i.maxDate
          WHERE campaignId IN (20, 24)
            AND o.systemDisposition = 'CONNECTED'
            AND o.dispositionCode NOT IN ('Sold', 'NIT', 'Not happy with service', 'Support call', 
              'Dont call again', 'Refund call')
            AND o.createdAt < NOW() - INTERVAL 3 DAY`,
    v4: ` SELECT DISTINCT o.phone
          FROM customerSupportCallDetails AS o
          INNER JOIN (
            SELECT phone, max(createdAt) AS maxDate
              FROM customerSupportCallDetails AS t
              WHERE systemDisposition = 'CONNECTED'
                AND campaignId IN (17, 24, 45)
                AND createdAt > NOW() - INTERVAL 32 DAY
                AND phone NOT IN(
                  SELECT DISTINCT phone
                  FROM customerSupportCallDetails
                  WHERE campaignId IN (17, 24, 45)
                    AND systemDisposition = 'CONNECTED'
                    AND dispositionCode IN ('Sold', 'NIT', 'Not happy with service', 'Support call', 
                      'Dont call again', 'Refund call')
                    AND phone IN (
                      SELECT DISTINCT phone
                      FROM customerSupportCallDetails
                      WHERE campaignId IN (24, 45)
                        AND systemDisposition = 'CONNECTED'
                        AND dispositionCode NOT IN ('Sold', 'NIT', 'Not happy with service', 'Support call', 
                          'Dont call again', 'Refund call')
                        AND (
                          (createdAt > NOW() - INTERVAL 5 DAY AND createdAt < NOW() - INTERVAL 4 DAY)
                        )
                    )
                    AND createdAt > NOW() - INTERVAL 31 DAY
                )
                AND phone IN (
                  SELECT DISTINCT phone
                  FROM customerSupportCallDetails
                  WHERE campaignId IN (24, 45)
                    AND systemDisposition = 'CONNECTED'
                    AND dispositionCode NOT IN ('Sold', 'NIT', 'Not happy with service', 'Support call',
                      'Dont call again', 'Refund call')
                    AND (
                      (createdAt > NOW() - INTERVAL 5 DAY AND createdAt < NOW() - INTERVAL 4 DAY)
                    )
                )
            GROUP BY phone
          ) i ON o.phone = i.phone
          AND o.createdAt = i.maxDate
          WHERE campaignId IN (20, 24, 45)
            AND o.systemDisposition = 'CONNECTED'
            AND o.dispositionCode NOT IN ('Sold', 'NIT', 'Not happy with service', 'Support call', 
              'Dont call again', 'Refund call')
            AND o.createdAt < NOW() - INTERVAL 3 DAY`
  }
}
