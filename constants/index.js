module.exports = {
  ...require('./consult'),
  ...require('./content'),
  ...require('./date'),
  ...require('./disposition'),
  ...require('./coupon'),
  ...require('./order'),
  ...require('./patient'),
  ...require('./leadScore'),
  ...require('./leadAttributeScore'),
  ...require('./exotel'),
  ...require('./ameyo')
}