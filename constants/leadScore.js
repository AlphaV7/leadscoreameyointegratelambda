module.exports = {
  LEAD_SCORE_TABLE_ATTRIBUTES: ['patientId', 'score', 'version', 'service', 'campaignName'],
  LEAD_SCORE_PATIENT_ATTRIBUTE: 'patientId'
}