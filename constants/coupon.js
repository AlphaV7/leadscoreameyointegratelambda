module.exports = {
  COUPON_CODE_ATTRIBUTES: ['code', 'validTill', 'active', 'reusable', 'name'],
  REMOVE_SIMILAR_COUPON_CODE: ['%swiggy%', '%SWIGGY%', '%ola%', '%OLA%', '%Swiggy%', '%Ola%'],
  REMOVE_COUPON_CODE:  ['SWIGGY3', 'Swiggy3', 'swiggy3', 'SWIGGY1', 'Swiggy1', 'swiggy1', 'OLA3', 'Ola3',
    'ola3', 'OLA1', 'Ola1', 'ola1', 'OLA12', 'Ola12', 'ola12', 'OLA6', 'Ola6', 'ola6', 'Swiggy3', 'Uber1',
    'uber1', 'UBER1', 'Mahaveer1', 'MAHAVEER1', 'mahaveer1', 'Mobisy1', 'MOBISY1', 'mobisy1'],
  GOLD_COUPON_PATTERN: 'HEALTHY'
};