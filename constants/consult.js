module.exports = {
  EXCLUDE_TOPICS: ['Prescription Validation', 'Cardiac Lab Analysis', 'Cardiac Risk Self Test',
    'Depression Self Test', 'Diabetes Self Test', 'Pregnancy Self Test', 'Thyroid Self Test'],
  FAMILY_MEMBER: {
    unique: ['me', 'Me', 'myself', 'Myself', 'husband', 'Husband', 'wife', 'Myself', 'partner', 'Partner',
      'Father', 'Wife', 'mother', 'Mother', 'father', 'पत्नी', 'पिता', 'माता', 'पति', '?????', '???'],
    multiple: ['brother', 'Brother', 'Daugther', 'Daughter', 'Friend', 'friend', 'sister', 'Sister', 'daughter',
      'daugther', 'son', 'Son', "बेटी", "बेटा", 'अन्य', 'Other', 'other', '????']
  },
  CONSULT_PATIENT_ATTRIBUTE: 'patient',
  CONSULT_GENDER_TYPE: {
    male: ['Male', 'MALE', 'male', 'M'],
    female: ['Female', 'FEMALE', 'female', 'F']
  },
  CONSULT_TOPIC: {
    lifeStyleAdvice: 'Life Style Advice',
    weightManagement: 'Weight management',
    buyMedicine: 'Buy Medicines & Health Products',
    bookLabTest: 'Book a Lab Test'
  },
  TOPIC_LIKE: {
    labSelfServe: 'labSelfServe%'
  }
};