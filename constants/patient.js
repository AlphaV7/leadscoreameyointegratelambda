module.exports = {
  ASSOCIATION_ATTRIBUTES: ['id', 'phonenumber', 'registeredPhonenumber'],
  PATIENT_PHONE_ATTRIBUTES: ['id', 'phonenumber', 'registeredPhonenumber']
}
