require('module-alias/register');

const { consultLeadController, followUpLeadController, paymentLeadController } = require('@controllers'),
  { logService, ameyoService } = require('@services'),
  { leadScoreUtil } = require('@utils'),
  { logHelper } = require('@helpers'),
  bugsnag = require('@bugsnag');

const processLeads = async () => {
  let recentPaidConsultLeadScoreList = [],
    pastPaidConsultLeadScoreList = [],
    followUpLeadScoreList = [],
    notConnectedScoreList = [],
    failedPaymentLeadScoreList = [];

  followUpLeadScoreList = await followUpLeadController.getFollowUpLeads();
  notConnectedScoreList = await followUpLeadController.getCallNotConnectedLeads();

  recentPaidConsultLeadScoreList = await consultLeadController.getAssistRecentPaidLeads();
  pastPaidConsultLeadScoreList = await consultLeadController.getAssistPastPaidLeads();

  failedPaymentLeadScoreList = await paymentLeadController.getFailedPaymentLeads();

  leadScoreUtil.prioritizeCampaignLeads(recentPaidConsultLeadScoreList, pastPaidConsultLeadScoreList,
    failedPaymentLeadScoreList, followUpLeadScoreList, notConnectedScoreList);

  await Promise.all([
    logService.logRecentPaidConsultLeadScoreList(recentPaidConsultLeadScoreList),
    logService.logPastPaidConsultLeadScoreList(pastPaidConsultLeadScoreList),
    logService.logFollowUpLeadScoreList(followUpLeadScoreList),
    logService.logNotConnectedCallsLeadScoreList(notConnectedScoreList),
    logService.logFailedPaymentLeadScoreList(failedPaymentLeadScoreList)
  ]);

  await ameyoService.postCampaignLeadListToAmeyo([], [], [], [], [], [],
    failedPaymentLeadScoreList, recentPaidConsultLeadScoreList, pastPaidConsultLeadScoreList);

  return;
}

exports.handler = async (event) => {
  try {
    logHelper.log('Process Started');

    await processLeads();

    logHelper.log('Process Ended');

    return { succes :1 };
  }
  catch (err) {
    logHelper.log("Error handler " + err.stack);
    bugsnag.notify(err);

    return { success: 0, Error: err };
  }
};
