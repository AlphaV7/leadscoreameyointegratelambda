const ENV = process.env.NODE_ENV || 'production';

const scorecardV2Config = require('./scorecardV2.json'),
  appConfig = require(`./${ENV}Config.json`),
  ameyoCampaignConfig = require('./ameyoCampaign.json'),
  bugsnagConfig = require('./bugsnag.js');

console.log('NODE_ENV: ', ENV);
console.log('App config: ', appConfig);
console.log('Ameyo Campaign Conig:', ameyoCampaignConfig);

module.exports = {
  ...scorecardV2Config,
  ...appConfig,
  ...ameyoCampaignConfig,
  bugsnag: bugsnagConfig
}