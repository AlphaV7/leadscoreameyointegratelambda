//Bugsnag Registration
const bugsnag = require("bugsnag");

bugsnag.register("0660b5dc9fc8082c82c2ac81e5f24f88", {
  onUncaughtError: function(err) {
    console.error(err.stack || err);
  }
});

module.exports = bugsnag;