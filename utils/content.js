const _ = require('lodash');

const { contents } = require('@models'),
  { queryHelper } = require('@helpers');

const getContentDataWrapper = (params) => {
  const { attributes, patientId, createdAtRange, updatedAtRange, adminMessageType, read, delivered,
      topic, type, consultationId, readAtRange, deliveredAtRange, $or, groupBy } = params,
    query = {
      where: {},
      raw: true
    };

  attributes && _.set(query, 'attributes', queryHelper.generateAttributeQueryObject(attributes));

  patientId && _.set(query, 'where.patientId', queryHelper.generateEqualQueryObject(patientId));
  adminMessageType && _.set(query, 'where.adminMessageType', queryHelper.generateEqualQueryObject(adminMessageType));
  read && _.set(query, 'where.read', queryHelper.generateEqualQueryObject(read));
  delivered && _.set(query, 'where.delivered', queryHelper.generateEqualQueryObject(delivered));
  topic && _.set(query, 'where.topic', queryHelper.generateEqualQueryObject(topic));
  type && _.set(query, 'where.type', queryHelper.generateEqualQueryObject(type));
  consultationId && _.set(query, 'where.consultationId', queryHelper.generateEqualQueryObject(consultationId));

  createdAtRange && _.set(query, 'where.createdAt', queryHelper.generateRangeQueryObject(createdAtRange));
  updatedAtRange && _.set(query, 'where.updatedAt', queryHelper.generateRangeQueryObject(updatedAtRange));
  readAtRange && _.set(query, 'where.readAt', queryHelper.generateRangeQueryObject(readAtRange));
  deliveredAtRange && _.set(query, 'where.deliveredAt', queryHelper.generateRangeQueryObject(deliveredAtRange));

  $or && _.set(query, 'where.$or', $or);
  groupBy && _.set(query, 'group', groupBy);

  return query;
}

const fetchContentData = async (queryParams) => {
  let query = {};

  if (!queryParams) {
    return;
  }

  query = getContentDataWrapper(queryParams);

  return await contents.findAll(query);
}

module.exports = {
  fetchContentData,
  getContentDataWrapper
};
