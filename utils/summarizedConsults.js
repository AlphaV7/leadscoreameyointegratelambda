const _ = require('lodash'),
  Sequelize = require('sequelize');

const { consults } = require('@models');

const fetchSummarizedConsultPatientList = async (params) => {
  const attributes = [
    [Sequelize.fn('DISTINCT', Sequelize.col('patient')), 'patientId'],
    ..._.get(params, 'attributes', [])
  ];

  return await fetchSummarizedConsults({attributes, ..._.omit(params, 'attributes')});
}

const fetchSummarizedConsults = async (params) => {
  const { summarizedAfter, summarizedBefore, excludeTopics, excludeSimilarTopic, attributes } = params,
    query = {
      where: {
        isSummarized: 1,
        valid: 1,
        acceptedConsultation: { $ne: null }
      },
      raw: true
    };

  summarizedAfter && _.set(query, 'where.summarizedTime.$gt', summarizedAfter );
  summarizedBefore && _.set(query, 'where.summarizedTime.$lt', summarizedBefore );

  Array.isArray(excludeTopics) && _.set(query, 'where.topic', { $notIn: excludeTopics });
  typeof excludeSimilarTopic === 'string' && _.assign(query.where.topic, { $notLike: excludeSimilarTopic });
  Array.isArray(attributes) && _.set(query, 'attributes', attributes);

  return await consults.findAll(query);
}

module.exports = {
  fetchSummarizedConsults,
  fetchSummarizedConsultPatientList
}
