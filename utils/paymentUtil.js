const _ = require('lodash'),
  Sequelize = require('sequelize');

const { orders, receivedPayment } = require('@models'),
  { queryHelper } = require('@helpers'),
  { CONSULT_TOPIC, ORDER_PAYMENT_SUCCESS_STATUS, ONE_DAY } = require('@constants');

const fetchOrderDataWrapper = async (params) => {
  const { attributes, patientId, couponCodeLike, couponCodeEqual, createdAtRange, updatedAtRange,
      couponCodeNotLike, paymentStatus, paymentType, refunded, $or, groupBy, packageId, topic, totalAmountRange,
      netPaidAmountRange, consultationIdNotEqual } = params,
    query = {
      where: {},
      raw: true
    };

  attributes && _.set(query, 'attributes', queryHelper.generateAttributeQueryObject(attributes));

  patientId && _.set(query, 'where.patientId', queryHelper.generateEqualQueryObject(patientId));
  couponCodeLike && _.set(query, 'where.couponCode', queryHelper.generateLikeQueryObject(couponCodeLike));
  couponCodeNotLike && _.set(query, 'where.couponCode', queryHelper.generateNotLikeQueryObject(couponCodeLike));
  couponCodeEqual && _.set(query, 'where.couponCode', queryHelper.generateEqualQueryObject(couponCodeEqual));
  paymentStatus && _.set(query, 'where.paymentStatus', queryHelper.generateEqualQueryObject(paymentStatus));
  paymentType && _.set(query, 'where.paymentType', queryHelper.generateEqualQueryObject(paymentType));
  packageId && _.set(query, 'where.packageId', queryHelper.generateEqualQueryObject(packageId));
  topic && _.set(query, 'where.topic', queryHelper.generateEqualQueryObject(topic));

  typeof refunded !== 'undefined' && _.set(query, 'where.refunded', queryHelper.generateEqualQueryObject(refunded));
  typeof consultationIdNotEqual !== 'undefined' && _.set(query, 'where.consultationId',
    queryHelper.generateNotEqualQueryObject(consultationIdNotEqual));

  netPaidAmountRange && _.set(query, 'where.netPaidAmount', queryHelper.generateRangeQueryObject(netPaidAmountRange));
  totalAmountRange && _.set(query, 'where.totalAmount', queryHelper.generateRangeQueryObject(totalAmountRange));
  createdAtRange && _.set(query, 'where.createdAt', queryHelper.generateRangeQueryObject(createdAtRange));
  updatedAtRange && _.set(query, 'where.updatedAt', queryHelper.generateRangeQueryObject(updatedAtRange));

  $or && _.set(query, 'where.$or', $or);
  groupBy && _.set(query, 'group', groupBy);

  return await fetchOrderData(query);
}

const generateReceivedPaymentWrapper = (params = {}) => {
  const { attributes, paymentStatus, topic, consultationId, contentId, patientId, couponCode, topicNotLike,
      createdAtRange, couponCodeNotLike, topicNotEqual, paymentStatusNotEqual } = params,
    query = {
      raw: true,
      where: {}
    };

  attributes && _.set(query, 'attributes', queryHelper.generateAttributeQueryObject(attributes));

  paymentStatus && _.set(query, 'where.paymentStatus', queryHelper.generateEqualQueryObject(paymentStatus));
  paymentStatusNotEqual && _.set(query, 'where.paymentStatus', queryHelper.generateNotEqualQueryObject(
    paymentStatusNotEqual));
  consultationId && _.set(query, 'where.consultationId', queryHelper.generateEqualQueryObject(consultationId));
  contentId && _.set(query, 'where.contentId', queryHelper.generateEqualQueryObject(contentId));
  patientId && _.set(query, 'where.patientId', queryHelper.generateEqualQueryObject(patientId));

  couponCode && _.set(query, 'where.couponCode', queryHelper.generateEqualQueryObject(couponCode));
  couponCodeNotLike && _.set(query, 'where.couponCode', { ..._.get(query, 'where.couponCode', {}),
    ...queryHelper.generateNotLikeQueryObject(couponCodeNotLike)
  });

  topic && _.set(query, 'where.topic', queryHelper.generateEqualQueryObject(patientId));
  topicNotEqual && _.set(query, 'where.topic', { ..._.get(query, 'where.topic', {}),
    ...queryHelper.generateNotEqualQueryObject(topicNotEqual)
  });
  topicNotLike && _.set(query, 'where.topic', { ..._.get(query, 'where.topic', {}),
    ...queryHelper.generateNotLikeQueryObject(topicNotLike)
  });

  createdAtRange && _.set(query, 'where.createdAt', queryHelper.generateRangeQueryObject(createdAtRange));

  return query;
}

const fetchDietChartData = async (params = {}) => {
  const queryParams = {
    topic: CONSULT_TOPIC.weightManagement,
    consultationIdNotEqual: 0,
    refunded: false,
    totalAmountRange: {
      below: 100,
      above: 30
    },
    ...params
  };

  return await fetchOrderDataWrapper(queryParams);
}

const fetchLifeStyleAdviceData = async (params = {}) => {
  const queryParams = {
    topic: CONSULT_TOPIC.lifeStyleAdvice,
    consultationIdNotEqual: 0,
    refunded: false,
    totalAmountRange: {
      below: 100,
      above: 30
    },
    ...params
  };

  return await fetchOrderDataWrapper(queryParams);
}

const fetchPatientConsultCount = async (params) => {
  const attributes = [
    [Sequelize.literal('COUNT(DISTINCT(consultationId))'), 'consultationCount'],
    ..._.get(params, 'attributes', [])
  ];

  return await fetchOrderDataWrapper({attributes, ..._.omit(params, 'attributes')});
}

const fetchPatientNetPayment = async (params) => {
  const attributes = [
    [Sequelize.fn('sum', Sequelize.col('netPaidAmount')), 'netPaidAmount'],
    ..._.get(params, 'attributes', [])
  ];

  return await fetchOrderDataWrapper({attributes, ..._.omit(params, 'attributes')});
}

const fetchOrderData = async (queryParams) => {
  if (!queryParams) {
    return;
  }

  return await orders.findAll(queryParams);
}

const fetchReceivedPaymentData = async (params) => {
  let queryParams = {};

  queryParams = generateReceivedPaymentWrapper(params);

  if (!params) {
    return [];
  }

  return await receivedPayment.findAll(queryParams);
}

const fetchPastFailedPaymentData = async (params) => {
  const queryParams = {
    paymentStatusNotEqual: ORDER_PAYMENT_SUCCESS_STATUS,
    createdAtRange: {
      above: new Date().setTime(new Date().getTime() - ONE_DAY),
      below: new Date().setTime(new Date().getTime())
    },
    ...params
  };

  return await fetchReceivedPaymentData(queryParams);
}

module.exports = {
  generateReceivedPaymentWrapper,
  fetchDietChartData,
  fetchLifeStyleAdviceData,
  fetchOrderData,
  fetchOrderDataWrapper,
  fetchPastFailedPaymentData,
  fetchPatientConsultCount,
  fetchPatientNetPayment,
  fetchReceivedPaymentData
};
