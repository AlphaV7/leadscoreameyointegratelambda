const _ = require('lodash');

const { couponCodes } = require('@models'),
  { queryHelper, logHelper } = require('@helpers');

const getPatientIdFromCouponCode = (couponCode) => {
  let patientId = [];

  if (Array.isArray(couponCode)) {
    couponCode.forEach((code) => {
      patientId.push(getPatientIdFromCouponCode(code));
    });
  }

  if (typeof couponCode === 'string') {
    try {
      patientId = parseInt(couponCode.replace(/[^0-9]+/ig, ''))
    } catch (err) {
      logHelper.log('Unable to parse couponCode: ', couponCode);
    }
  }

  return patientId;
}

const filterGoldPackageUser = async (params) => {
  const { patientList } = params,
    HEALTHY_CODE = 'HEALTHY';
  let goldPackagePatient = [];

  typeof patientList === 'string' && _.set(params, 'codeEqual', [
    `${HEALTHY_CODE}${patientList}`,
    `${HEALTHY_CODE}${patientList}A`,
    `${HEALTHY_CODE}${patientList}B`,
    `${HEALTHY_CODE}${patientList}C`,
    `${HEALTHY_CODE}A${patientList}`,
    `${HEALTHY_CODE}B${patientList}`,
    `${HEALTHY_CODE}C${patientList}`
  ]);

  if (Array.isArray(patientList)) {
    _.set(params, 'codeEqual', []);

    patientList.forEach((patient) => {
      params.codeEqual.push(...[
        `${HEALTHY_CODE}${patient}`,
        `${HEALTHY_CODE}${patient}A`,
        `${HEALTHY_CODE}${patient}B`,
        `${HEALTHY_CODE}${patient}C`,
        `${HEALTHY_CODE}A${patient}`,
        `${HEALTHY_CODE}B${patient}`,
        `${HEALTHY_CODE}C${patient}`
      ]);
    });
  }

  return fetchCouponWrapper(params)
    .then((couponDataList) => {
      couponDataList.forEach((couponData) => {
        let couponCode = couponData.code,
          patientId = getPatientIdFromCouponCode(couponCode);

        goldPackagePatient.push(patientId);
      });

      return goldPackagePatient;
    }, (err) => {
      logHelper.log('Unable to filter gold package user. Error: ', err);
      return [];
    });
}

const fetchCouponWrapper = async (params) => {
  const { attributes, codeEqual, codeLike, active, reusable, validFrom, validTo,
      createdAfter, createdBefore, updatedAfter, updatedBefore } = params,
    query = {
      raw: true,
      where: {}
    };

  Array.isArray(attributes) && _.set(query, 'attributes', attributes);

  reusable === false && _.set(query, 'where.reusable', { $eq: 0 });
  reusable === true && _.set(query, 'where.reusable', { $gt: 0 });
  active === false && _.set(query, 'where.active', { $eq: 0 });
  active === true && _.set(query, 'where.active', { $gt: 0 });

  createdAfter && _.set(query, 'where.createdAt.$gte', createdAfter);
  createdBefore && _.set(query, 'where.createdAt.$lte', createdBefore);
  updatedAfter && _.set(query, 'where.updatedAt.$gte', updatedAfter);
  updatedBefore && _.set(query, 'where.updatedAt.$lte', updatedBefore);

  validFrom && _.set(query, 'where.validTill.$gte', validFrom);
  validTo && _.set(query, 'where.validTill.$lte', validTo);
  (validFrom || validTo) && _.set(query, 'where.validTill.$ne', null);

  codeLike && _.set(query, 'where.code', queryHelper.generateLikeQueryObject(codeLike));
  codeEqual && _.set(query, 'where.code', queryHelper.generateEqualQueryObject(codeEqual));

  Array.isArray(codeEqual) && _.set(query, 'where.code.$in', codeEqual);

  return await fetchCoupon(query);
}

const fetchCoupon = async (queryParams) => {
  if (!queryParams) {
    return;
  }

  return await couponCodes.findAll(queryParams);
}

module.exports = {
  filterGoldPackageUser,
  fetchCouponWrapper,
  fetchCoupon,
  getPatientIdFromCouponCode
}
