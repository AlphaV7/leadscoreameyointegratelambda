const _ = require('lodash');

const { Subscription } = require('@models'),
  { readReplicaSequelize } = require('@sequelize'),
  { queryHelper } = require('@helpers'),
  { ACTIVE_SUBSCRIPTION_QUERY } = require('@constants');

const getSubscriptionDataWrapper = (params) => {
  const { attributes, patientId, orderId, status, purchaseType, createdAtRange, updatedAtRange, validFromRange,
      validTillRange, packageType, packageId, $or, groupBy } = params,
    query = {
      where: {},
      raw: true
    };

  attributes && _.set(query, 'attributes', queryHelper.generateAttributeQueryObject(attributes));

  patientId && _.set(query, 'where.patientId', queryHelper.generateEqualQueryObject(patientId));
  orderId && _.set(query, 'where.orderId', queryHelper.generateEqualQueryObject(orderId));
  purchaseType && _.set(query, 'where.purchaseType', queryHelper.generateEqualQueryObject(purchaseType));
  packageType && _.set(query, 'where.packageType', queryHelper.generateEqualQueryObject(packageType));
  packageId && _.set(query, 'where.packageId', queryHelper.generateEqualQueryObject(packageId));

  typeof status !== 'undefined' && _.set(query, 'where.status', queryHelper.generateEqualQueryObject(status));

  createdAtRange && _.set(query, 'where.createdAt', queryHelper.generateRangeQueryObject(createdAtRange));
  updatedAtRange && _.set(query, 'where.updatedAt', queryHelper.generateRangeQueryObject(updatedAtRange));
  validFromRange && _.set(query, 'where.validFrom', queryHelper.generateRangeQueryObject(validFromRange));
  validTillRange && _.set(query, 'where.validTill', queryHelper.generateRangeQueryObject(validTillRange));

  $or && _.set(query, 'where.$or', $or);
  groupBy && _.set(query, 'group', groupBy);

  return query;
}

const fetchSubscriptionData = async (queryParams) => {
  let query = {};

  if (!queryParams) {
    return;
  }

  query = getSubscriptionDataWrapper(queryParams);

  return await Subscription.findAll(query);
}

const fetchRecentSubscriptionData = async (patientIdList, queryType = 'SELECT') => {
  let queryResult = [];

  if (!patientIdList || !Array.isArray(patientIdList) || !patientIdList.length) {
    return [];
  }

  queryResult = await readReplicaSequelize.query(ACTIVE_SUBSCRIPTION_QUERY.v1, {
    type: queryType,
    replacements: {
      patientIdList
    },
    raw: true
  });

  return queryResult;
}

module.exports = {
  fetchRecentSubscriptionData,
  fetchSubscriptionData,
  getSubscriptionDataWrapper
};
