const request = require('request-promise-native'),
  _ = require('lodash'),
  querystring = require('querystring');

const getRequestObject = (options = {}, requestData = {}) => {
  const { url, method } = options,
    { header, body, query } = requestData,
    requestObject = {
      method: method,
      uri: url
    }

  if (header) {
    _.set(requestObject, 'headers', header);
  }

  if (body) {
    _.set(requestObject, 'body', body);
    typeof body === 'object' && _.set(requestObject, 'json', true);
  }

  if (query) {
    _.set(requestObject, 'uri', `${requestObject.uri}?${querystring.stringify(query)}`);
  }

  return requestObject;
}

const fetchData = async (options, requestData) => {
  const requestConfig = getRequestObject(options, requestData);

  return request(requestConfig);
}

module.exports = {
  getRequestObject,
  fetchData
}
