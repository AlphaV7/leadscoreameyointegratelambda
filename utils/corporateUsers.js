const _ = require('lodash');

const { corporateUsers } = require('@models'),
  { queryHelper } = require('@helpers');

const fetchCorporateUsersWrapper = async (params) => {
  const { attributes, source, patientId, corporateUserUniqueId, name, phonenumber, age, gender,
      points, createdAtRange, updatedAtRange, patientIdNotEqual } = params,
    query = {
      where: {},
      raw: true
    };

  attributes && _.set(query, 'attributes', queryHelper.generateAttributeQueryObject(attributes));

  source && _.set(query, 'where.source', queryHelper.generateEqualQueryObject(source));
  patientId && _.set(query, 'where.patientid', queryHelper.generateEqualQueryObject(patientId));
  name && _.set(query, 'where.name', queryHelper.generateEqualQueryObject(name));
  phonenumber && _.set(query, 'where.phonenumber', queryHelper.generateEqualQueryObject(phonenumber));
  age && _.set(query, 'where.age', queryHelper.generateEqualQueryObject(age));
  gender && _.set(query, 'where.gender', queryHelper.generateEqualQueryObject(gender));
  points && _.set(query, 'where.points', queryHelper.generateEqualQueryObject(points));
  corporateUserUniqueId && _.set(query, 'where.corporateUserUniqueId',
    queryHelper.generateEqualQueryObject(corporateUserUniqueId));

  patientIdNotEqual && _.set(query, 'where.patientid', queryHelper.generateNotEqualQueryObject(patientId));

  createdAtRange && _.set(query, 'where.createdAt', queryHelper.generateRangeQueryObject(createdAtRange));
  updatedAtRange && _.set(query, 'where.updatedAt', queryHelper.generateRangeQueryObject(updatedAtRange));

  return await fetchCorporateUsers(query);
}

const fetchCorporateUsers = async (queryParams) => {
  if (!queryParams) {
    return;
  }

  return await corporateUsers.findAll(queryParams);
}

module.exports = {
  fetchCorporateUsersWrapper,
  fetchCorporateUsers
};
