const fs = require('fs');

// Read from this directory and add them as exports
// This way you can just reference
fs.readdirSync('./utils').forEach(function(file){
  if (file.indexOf(".js") > -1 && file !== 'index.js') {
    if (file.indexOf('Util') > -1 || file.indexOf('util') > -1) {
      exports[file.replace('.js', '')] = require('./' + file);
    } else {
      exports[file.replace('.js', 'Util')] = require('./'+file);
    }
  }
});