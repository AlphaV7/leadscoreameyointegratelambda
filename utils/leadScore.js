const _ = require('lodash');

const { LeadScore } = require('@models'),
  { commonHelper } = require('@helpers'),
  scoreCardUtil = require('./scoreCard');

const prioritizeCampaignLeads = (...leadArrayList) => {
  const leadPatientList = [],
    leadListCount = leadArrayList.length;

  leadArrayList.forEach((leadList, index) => (
    leadPatientList.push(commonHelper.transformObjectToNumberList(leadList, 'patientId'))
  ));

  leadPatientList.forEach((patientIdList, index) => {
    for (let currentIndex = index + 1; currentIndex < leadListCount; currentIndex++) {
      _.pull(leadPatientList[currentIndex], ...patientIdList);
    }
  });

  leadArrayList.forEach((leadList, index) => {
    let leadListDuplicate = Array.from(leadList);

    leadArrayList[index].length = 0;
    leadListDuplicate.forEach((leadData) => (
      leadPatientList[index].indexOf(parseInt(leadData.patientId)) !== -1 && leadArrayList[index].push(leadData)
    ));
  });
}

const getLeadScoreWriteObject = (params) => {
  const { patientId, score, version, service, campaignName, meta } = params,
    writeObject = {
      patientId,
      score,
      campaignName: campaignName,
      version: version || scoreCardUtil.getScoreCardVersion(),
      service: service || scoreCardUtil.getScoreCardServiceName()
    };

  _.set(writeObject, `meta`, JSON.stringify(meta || params));

  return writeObject;
}

const writeScoreToDB = async (writeData) => {
  const writeDataList = Array.isArray(writeData) ? writeData : [writeData];

  if (!writeDataList || !writeDataList.length) {
    return;
  }

  return await LeadScore.bulkCreate(writeDataList);
}

module.exports = {
  prioritizeCampaignLeads,
  getLeadScoreWriteObject,
  writeScoreToDB
};
