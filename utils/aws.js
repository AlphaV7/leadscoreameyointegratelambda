const AWS = require('aws-sdk');

const { aws } = require('@config'),
  { logHelper } = require('@helpers'),
  util = require('util'),
  exec = util.promisify(require('child_process').exec);

AWS.config.update(aws);

const sqs = new AWS.SQS(),
  ses = new AWS.SES(),
  AWS_COMMAND_MAP = {
    fetchStreamList: `aws logs describe-log-streams --log-group-name`,
    fetchStreamData: `aws logs get-log-events --log-group-name`
  },
  AWS_LOG_GROUP_MAP = {
    leadScoreAmeyoIntegrate: '/aws/lambda/leadScoreAmeyoIntegrateLambda'
  }

const getAwsConfig = () => (aws);

const postParamsToSQS = (params) => {
  return new Promise((resolve, reject) => {
    sqs.sendMessage(params, (err, data) => {
      if (err) {
        logHelper.log('Unable to post to Queue. Error: ', err);
        return reject(err);
      }

      logHelper.log('Successfully posted. Data: ', data);
      return resolve(data);
    });
  });
}

const postEmailToSES = async (params) => {
  if (!params) {
    return;
  }

  return await ses.sendEmail(params).promise();
}

const fetchCloudWatchLogs = async (params) => {
  const { logGroupName, createdAtRange } = params,
    { above, below } = createdAtRange,
    fetchLogStreamListCommand = `${AWS_COMMAND_MAP.fetchStreamList} ${AWS_LOG_GROUP_MAP[logGroupName]}`;

  let commandOutput,
    fetchedLogStreamList = [],
    dateToLogMapping = {};

  if (!logGroupName) {
    return;
  }

  commandOutput = await exec(fetchLogStreamListCommand, { maxBuffer: 1024 * 500 });

  fetchedLogStreamList = JSON.parse(commandOutput.stdout);

  if (!fetchedLogStreamList || !fetchedLogStreamList.logStreams) {
    return;
  }

  await Promise.all(fetchedLogStreamList.logStreams.map(async (currentLogStreamData) => {
    const { logStreamName, creationTime } = currentLogStreamData;
    let isValidStream = false,
      currentLogs;

    isValidStream = above ? (parseInt(creationTime) - new Date(above).getTime() >= 0) : true;
    isValidStream = isValidStream && (below ? (new Date(below).getTime() - parseInt(creationTime) >= 0) : true);

    if (!isValidStream) {
      return;
    }

    currentLogs = await fetchCloudWatchLogFromLogStream({ logGroupName,
      logStreamName: logStreamName.replace(`[$LATEST]`, `[\\$LATEST]`) });

    dateToLogMapping[new Date(creationTime).toISOString()] = (JSON.parse(currentLogs));
  }));

  return dateToLogMapping;
}

const fetchCloudWatchLogFromLogStream = async (params) => {
  const { logGroupName, logStreamName } = params,
    fetchCommand = `${AWS_COMMAND_MAP.fetchStreamData} ${AWS_LOG_GROUP_MAP[logGroupName]} 
      --log-stream-name ${logStreamName}`;

  if (!logGroupName || !logStreamName) {
    return `Log group name and stream name is required`;
  }

  return (await exec(fetchCommand, { maxBuffer: 1024 * 1000 })).stdout;
}

module.exports = {
  fetchCloudWatchLogs,
  fetchCloudWatchLogFromLogStream,
  postEmailToSES,
  postParamsToSQS,
  getAwsConfig
}