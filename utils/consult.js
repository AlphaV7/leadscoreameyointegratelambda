const _ = require('lodash');

const { consults, consultRating } = require('@models'),
  { FAMILY_MEMBER } = require('@constants'),
  { queryHelper, commonHelper } = require('@helpers');

/**
 * Calculate number of distinct family members through consultation data. Current Unique
 * and multiple member list depends on scoring logic and should be updated as per score card logic.
 * Calculates distinct family member count as per age of patient.
 * @param  {Array} consultDataList [Array of Objects from Consults table]
 * @return {Number}                 [Count of distinct family memeber]
 */
const getDistinctFamilyMemberCount = (consultDataList) => {
  let count = 0,
    forWhomCount = {};

  consultDataList.forEach(({ forwhom, age, createdAt }) => {
    const yearOfBirth = parseInt(new Date(createdAt).getFullYear()) - age;
    let isExistingPatient = false;

    if (!forWhomCount[forwhom]) {
      forWhomCount[forwhom] = []
    }

    isExistingPatient = forWhomCount[forwhom].indexOf(yearOfBirth) !== -1
      || forWhomCount[forwhom].indexOf(yearOfBirth - 1) !== -1
      || forWhomCount[forwhom].indexOf(yearOfBirth - 2) !== -1;

    !isExistingPatient && forWhomCount[forwhom].push(yearOfBirth);
  });

  Object.keys(forWhomCount).forEach((relation) => {
    if (FAMILY_MEMBER.unique.indexOf(relation) !== -1) {
      count += _.min([1, forWhomCount[relation].length]);
    } else if (FAMILY_MEMBER.multiple.indexOf(relation) !== -1){
      count += forWhomCount[relation].length;
    } else {
      count += _.min([1, forWhomCount[relation].length]);
    }
  });

  return count;
}

const getPatientAgeFromConsultData = (consultDataList) => {
  const patientRelationMap = {};
  let patientAgeList = [],
    patientAge;

  consultDataList.forEach(({ age, gender, name, forwhom}) => {
    if (!forwhom || !forwhom.length || forwhom === '') {
      forwhom = 'unknown'
    }

    if (!_.get(patientRelationMap, `${forwhom}`)) {
      _.set(patientRelationMap, `${forwhom}`, []);
    }

    patientRelationMap[forwhom].push(age);
  });

  patientAgeList = patientRelationMap['me'] || patientRelationMap['Me']
    || patientRelationMap['Myself'] || patientRelationMap['myself'];

  if (!patientAgeList || !patientAgeList.length) {
    let maxLength = 0,
      reqRelation = '';

    Object.keys(patientRelationMap).forEach((relation) => {
      if (patientRelationMap[relation].length >= maxLength) {
        maxLength = patientRelationMap[relation].length;
        reqRelation = relation;
      }
    });

    patientAgeList = patientRelationMap[reqRelation];
  }

  patientAge = commonHelper.calculateListMode(patientAgeList) || 28;

  return patientAge;
}

const generateQueryObject = (params) => {
  const { attributes, patientId, createdAfter, createdBefore, updatedAfter, updatedBefore, excludeTopics,
      excludeSimilarTopic, acceptedConsultation, acceptedBefore, acceptedAfter, status, forwhom, groupBy,
      distinctCount, countColumnName, $or, ageRange, valid, acceptedConsultationNotNull, forwhomNotNull,
      updatedAtRange, createdAtRange, acceptedAtRange, summarizedAtRange, gender } = params,
    query = {
      where: {},
      raw: true
    };

  Array.isArray(attributes) && _.set(query, 'attributes', attributes);
  distinctCount && _.set(query, 'distinct', distinctCount);
  countColumnName && _.set(query, 'col', countColumnName);
  groupBy && _.set(query, 'group', groupBy);

  patientId && _.set(query, 'where.patient', queryHelper.generateEqualQueryObject(patientId));
  gender && _.set(query, 'where.gender', queryHelper.generateEqualQueryObject(gender));

  createdAfter && _.set(query, 'where.createdAt.$gt',  createdAfter );
  updatedAfter && _.set(query, 'where.updatedAt.$gt', updatedAfter );
  acceptedAfter && _.set(query, 'where.acceptedConsultation.$gt', acceptedAfter);

  createdBefore && _.set(query, 'where.createdAt.$lt', createdBefore);
  updatedBefore && _.set(query, 'where.updatedAt.$lt', updatedBefore);
  acceptedBefore && _.set(query, 'where.acceptedConsultation$.$lt', acceptedBefore);

  updatedAtRange && _.set(query, 'where.updatedAt', queryHelper.generateRangeQueryObject(updatedAtRange));
  createdAtRange && _.set(query, 'where.createdAt', queryHelper.generateRangeQueryObject(createdAtRange));
  acceptedAtRange && _.set(query, 'where.acceptedConsultation', queryHelper.generateRangeQueryObject(acceptedAtRange));
  summarizedAtRange && _.set(query, 'where.summarizedTime', queryHelper.generateRangeQueryObject(summarizedAtRange));

  Array.isArray(excludeTopics) && _.set(query, 'where.topic', { $notIn: excludeTopics });
  typeof excludeSimilarTopic === 'string' && _.assign(query.where.topic, { $notLike: excludeSimilarTopic });
  ageRange && _.set(query, 'where.age', queryHelper.generateRangeQueryObject(ageRange));
  forwhom && _.set(query, 'where.forwhom', queryHelper.generateEqualQueryObject(forwhom));
  typeof forwhomNotNull !== 'undefined' && _.set(query, 'where.forwhom',
    { ..._.get(query, 'where.forwhom', {}), $ne: null });
  valid && _.set(query, 'where.valid', queryHelper.generateEqualQueryObject(valid));

  typeof status !== 'undefined' && _.set(query, 'where.status', { $eq: status });
  typeof acceptedConsultation !== 'undefined' && _.set(query, 'where.acceptedConsultation',
    { $eq: acceptedConsultation });
  acceptedConsultationNotNull && _.set(query, 'where.acceptedConsultation',
    { ..._.get(query, 'where.acceptedConsultation', {}), $ne: null });

  $or && _.set(query, 'where.$or', $or);

  return query;
}

const generateConsultRatingQuery = (params = {}) => {
  const { attributes, patientId, consultationId, appRatingRange, doctorRatingRange, createdAtRange } = params,
    query = {
      raw: true,
      where: {}
    };

  attributes && _.set(query, 'attributes', queryHelper.generateAttributeQueryObject(attributes));

  patientId && _.set(query, 'where.patientId', queryHelper.generateEqualQueryObject(patientId));
  consultationId && _.set(query, 'where.consultationId', queryHelper.generateEqualQueryObject(consultationId));

  appRatingRange && _.set(query, 'where.appRating', queryHelper.generateRangeQueryObject(appRatingRange));
  doctorRatingRange && _.set(query, 'where.doctorRating', queryHelper.generateRangeQueryObject(doctorRatingRange));
  createdAtRange && _.set(query, 'where.createdAt', queryHelper.generateRangeQueryObject(createdAtRange));

  return query;
}

const fetchConsultRatingData = async (params = {}) => {
  let query;

  query = generateConsultRatingQuery(params);

  if (!query) {
    return {};
  }

  return await consultRating.findAll(query);
}

const fetchConsultsWrapper = async (params) => {
  let query;

  query = generateQueryObject(params);

  return await fetchConsults(query);
}

const fetchConsultCountWrapper = async (params) => {
  let query  = generateQueryObject(params);

  return await fetchConsultsCount(query);
}

const fetchConsults = async (queryParams) => {
  if (!queryParams) {
    return;
  }

  return await consults.findAll(queryParams);
}

const fetchConsultsCount = async (queryParams) => {
  if (!queryParams) {
    return;
  }

  return await consults.count(queryParams);
}

const fetchBadAppRatingData = async (params = {}) => {
  const queryParams = {
    appRatingRange: {
      below: 3
    },
    ...params
  };

  return await fetchConsultRatingData(queryParams);
}

const fetchBadDoctorRatingData = async (params = {}) => {
  const queryParams = {
    doctorRatingRange: {
      below: 3
    },
    ...params
  };

  return await fetchConsultRatingData(queryParams);
}

module.exports = {
  fetchConsults,
  fetchConsultsWrapper,
  getDistinctFamilyMemberCount,
  getPatientAgeFromConsultData,
  fetchConsultRatingData,
  fetchConsultCountWrapper,
  fetchBadDoctorRatingData,
  fetchBadAppRatingData
}