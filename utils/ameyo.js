const config = require('@config');

const { ameyoCampaign, campaign } = config,
  AMEYO_DISPOSITION_CAMPAIGN_KEY = 'dispositionLeadCampaign',
  AMEYO_DISPOSITION_NOT_CONNECTED_KEY = 'dispositionNotConnectedLeadCampaign',
  AMEYO_LEAD_SCORE_CARD_CAMPAIGN = 'leadScoreV2Campaign',
  AMEYO_UNPAID_CONSULT_CAMPAIGN = 'unpaidConsultCampaign',
  AMEYO_SHORT_DURATION_CONNECTED_CAMPAIGN = 'shortDurationConnectedCampaign',
  AMEYO_FAILED_PAYMENT_CAMPAIGN = 'failedPaymentPackageUpsellCampaign',
  AMEYO_RECENT_PAID_CAMPAIGN = 'recentPaidCampaign',
  AMEYO_PAST_PAID_CAMPAIGN = 'pastPaidCampaign';

const getAmeyoCampaignConfig = () => (ameyoCampaign);

const getAmeyoDispositionCampaignKey = () => (AMEYO_DISPOSITION_CAMPAIGN_KEY);

const getAmeyoDispositionNotConnectedCampaignKey = () => (AMEYO_DISPOSITION_NOT_CONNECTED_KEY);

const getAmeyoLeadScoreV2CampaignKey = () => (AMEYO_LEAD_SCORE_CARD_CAMPAIGN);

const getAmeyoUnpaidConsultCampaignKey = () => (AMEYO_UNPAID_CONSULT_CAMPAIGN);

const getAmeyoShortDurationConnectedCampaignKey = () => (AMEYO_SHORT_DURATION_CONNECTED_CAMPAIGN);

const getAmeyoVernacularCampaignKey = (location) => (campaign.vernacularLead[location]);

const getAmeyoFailedPaymentCampaignKey = () => (AMEYO_FAILED_PAYMENT_CAMPAIGN);

const getAmeyoRecentPaidCampaignKey = () => (AMEYO_RECENT_PAID_CAMPAIGN);

const getAmeyoPastPaidCampaignKey = () => (AMEYO_PAST_PAID_CAMPAIGN);

module.exports = {
  getAmeyoCampaignConfig,
  getAmeyoDispositionCampaignKey,
  getAmeyoLeadScoreV2CampaignKey,
  getAmeyoDispositionNotConnectedCampaignKey,
  getAmeyoUnpaidConsultCampaignKey,
  getAmeyoShortDurationConnectedCampaignKey,
  getAmeyoVernacularCampaignKey,
  getAmeyoFailedPaymentCampaignKey,
  getAmeyoRecentPaidCampaignKey,
  getAmeyoPastPaidCampaignKey
};