const _ = require('lodash');

const { scoreCard } = require('@config');

const SCORE_ATTRIBUTE_MAP = {
    payment: 'payment',
    age: 'age',
    consultationCount: 'consultationCount',
    familyMemberCount: 'familyMemberCount'
  },
  { attributeScore, threshold, version, service } = scoreCard;

const getScoreCardThreshold = () => (threshold);

const getScoreCardVersion = () => (version);

const getScoreCardServiceName = () => (service);

const getAttributeScore = (configAttributeName, value, isNumber = true) => {
  let attributeList,
    requiredScore;

  attributeList = _.sortBy(Object.keys(attributeScore[configAttributeName]));

  if (isNumber) {
    value = parseInt(value);

    for (let index = 0; index < attributeList.length; index++) {
      attributeList[index] = parseInt(attributeList[index]);
    }
  }

  attributeList.forEach((listElement) => {
    if (listElement <= value) {
      requiredScore = _.get(attributeScore, `${configAttributeName}.${listElement}`);
    }
  });

  return requiredScore;
}

const getPaymentScore = (paymentAmount) => (
  getAttributeScore(SCORE_ATTRIBUTE_MAP.payment, paymentAmount)
)

const getAgeScore = (age) => (
  getAttributeScore(SCORE_ATTRIBUTE_MAP.age, age)
)

const getConsultationCountScore = (consultationCount) => (
  getAttributeScore(SCORE_ATTRIBUTE_MAP.consultationCount, consultationCount)
)

const getDistinctFamilyCount = (familyMemberCount) => (
  getAttributeScore(SCORE_ATTRIBUTE_MAP.familyMemberCount, familyMemberCount)
)

module.exports = {
  getScoreCardServiceName,
  getScoreCardVersion,
  getAttributeScore,
  getAgeScore,
  getScoreCardThreshold,
  getPaymentScore,
  getDistinctFamilyCount,
  getConsultationCountScore
}