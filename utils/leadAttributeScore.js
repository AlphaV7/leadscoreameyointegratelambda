const { leadAttributeScore } = require('@models');

const writeScoreToDB = async (writeData) => {
  const writeDataList = Array.isArray(writeData) ? writeData : [writeData];

  if (!writeDataList || !writeDataList.length) {
    return;
  }

  return await leadAttributeScore.bulkCreate(writeDataList);
}

module.exports = {
  writeScoreToDB
};
