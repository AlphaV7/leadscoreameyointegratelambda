const _ = require('lodash');

const { customerSupportCallDetails } = require('@models'),
  { readReplicaSequelize } = require('@sequelize'),
  { queryHelper } = require('@helpers');

const filterRequiredDisposition = (customerCallDetail, filterCallDetail, dispositionCode) => {
  const requiredDispositionCode = Array.isArray(dispositionCode) ? dispositionCode : [dispositionCode],
    invalidDispositionPhoneNumberList = [],
    requiredDispositionList = [];
  let currentDispositionCode,
    isInvalidDisposition;

  Object.keys(filterCallDetail).forEach((phonenumber) => {
    currentDispositionCode = filterCallDetail[phonenumber].dispositionCode;
    isInvalidDisposition = requiredDispositionCode.indexOf(currentDispositionCode) === -1 &&
        _.get(customerCallDetail, `${phonenumber}`, false);

    isInvalidDisposition && invalidDispositionPhoneNumberList.push(phonenumber);
  });

  Object.keys(customerCallDetail).forEach((phonenumber) => {
    isInvalidDisposition = invalidDispositionPhoneNumberList.indexOf(phonenumber) !== -1;

    !isInvalidDisposition && requiredDispositionList.push(customerCallDetail[phonenumber]);
  });

  return requiredDispositionList;
}

const fetchRecentPhoneCallDetails = async (params) => {
  const dispositionParams = {
      ...params,
      attributes: _.get(params, 'attributes', []).indexOf('phone') !== -1 ?
        params.attributes : [..._.get(params, 'attributes', []), 'phone'],
      orderBy: [
        ..._.get(params, 'orderBy', []),
        {
          column: 'updatedAt',
          desc: true
        }
      ]
    },
    dispositionDataList = await customerSupportCallDetailsWrapper(dispositionParams),
    dispositionPhoneMapping = {};

  dispositionDataList.forEach((dispositionElement) => {
    const { phone } = dispositionElement;

    !_.get(dispositionPhoneMapping, `${phone}`, false) &&
      _.set(dispositionPhoneMapping, `${phone}`, _.pick(dispositionElement, dispositionParams.attributes));
  });

  return dispositionPhoneMapping;
}

const fetchCustomerSupportCallDetailRawQuery = async (query, queryType = 'SELECT') => {
  if (!query || !query.length) {
    return {};
  }

  return readReplicaSequelize.query(query, { type: queryType });
}

const customerSupportCallDetailsWrapper = async (params) => {
  const { attributes, lastStatus, dstPhone, srcPhone, phone, campaignId, dispositionCode, callType, orderBy,
      createdAtRange, updatedAtRange, systemDisposition, systemDispositionNotIn, dispositionCodeNotIn,
      ringingTimeRange, talkTimeRange, $or } = params,
    query = {
      where: {},
      raw: true
    };

  attributes && _.set(query, 'attributes', queryHelper.generateAttributeQueryObject(attributes));

  lastStatus && _.set(query, 'where.lastStatus', queryHelper.generateEqualQueryObject(lastStatus));
  dispositionCode && _.set(query, 'where.dispositionCode', queryHelper.generateEqualQueryObject(dispositionCode));
  dstPhone && _.set(query, 'where.dstPhone', queryHelper.generateEqualQueryObject(dstPhone));
  srcPhone && _.set(query, 'where.srcPhone', queryHelper.generateEqualQueryObject(srcPhone));
  phone && _.set(query, 'where.phone', queryHelper.generateEqualQueryObject(phone));
  campaignId && _.set(query, 'where.campaignId', queryHelper.generateEqualQueryObject(campaignId));
  callType && _.set(query, 'where.callType', queryHelper.generateEqualQueryObject(callType));
  systemDisposition &&
    _.set(query, 'where.systemDisposition', queryHelper.generateEqualQueryObject(systemDisposition));

  dispositionCodeNotIn && _.set(query, 'where.dispositionCode',
    { ..._.get(query, 'where.dispositionCode', {}), ...queryHelper.generateNotEqualQueryObject(dispositionCodeNotIn) });

  systemDispositionNotIn && _.set(query, 'where.systemDisposition',
    { ..._.get(query, 'where.systemDisposition', {}),
      ...queryHelper.generateNotEqualQueryObject(systemDispositionNotIn) });

  createdAtRange && _.set(query, 'where.createdAt', queryHelper.generateRangeQueryObject(createdAtRange));
  updatedAtRange && _.set(query, 'where.updatedAt', queryHelper.generateRangeQueryObject(updatedAtRange));
  ringingTimeRange && _.set(query, 'where.ringingTime', queryHelper.generateRangeQueryObject(ringingTimeRange));
  talkTimeRange && _.set(query, 'where.talkTime', queryHelper.generateRangeQueryObject(talkTimeRange));

  $or && _.set(query, 'where.$or', $or);
  orderBy && _.set(query, 'order', queryHelper.generateQueryOrderByObject(orderBy));

  return await fetchCustomerSupportCallDetails(query);
}

const fetchCustomerSupportCallDetails = async (params) => {
  if (!params) {
    return;
  }

  return customerSupportCallDetails.findAll(params);
}

module.exports = {
  customerSupportCallDetailsWrapper,
  fetchCustomerSupportCallDetails,
  fetchRecentPhoneCallDetails,
  filterRequiredDisposition,
  fetchCustomerSupportCallDetailRawQuery
}
