const _ = require('lodash'),
  Promise = require('bluebird');

const { patient } = require('@models'),
  { queryHelper, commonHelper } = require('@helpers'),
  { PATIENT_PHONE_ATTRIBUTES } = require('@constants');

const fetchPatientIdFromPhoneNumberQuery = {
  phonenumber: true,
  registeredPhonenumber: true
};

const fetchPatientIdFromPhoneNumber = async (phoneNumberList, query = fetchPatientIdFromPhoneNumberQuery) => {
  const patientParams = {
    attributes: PATIENT_PHONE_ATTRIBUTES,
    parentIdNull: true
  };

  let patientPhoneData = [],
    patientRegisteredPhoneData = [],
    patientIdList = [],
    requiredPatientDataList = [];

  if (!phoneNumberList || !phoneNumberList.length) {
    return [];
  }

  [patientPhoneData, patientRegisteredPhoneData] = await Promise.all([
    patientDataWrapper({ ...patientParams, phonenumber: phoneNumberList }),
    patientDataWrapper({ ...patientParams, registeredPhonenumber: phoneNumberList })
  ]);

  query.phonenumber && requiredPatientDataList.push(...patientPhoneData);
  query.registeredPhonenumber && requiredPatientDataList.push(...patientRegisteredPhoneData);

  patientIdList = _.uniq(commonHelper.transformObjectToNumberList(requiredPatientDataList, 'id'));

  return patientIdList;
}

const filterPatientPhoneNumberList = (patientDetails = {}) => {
  const patientData = Array.isArray(patientDetails) ? patientDetails : [patientDetails];
  let phoneNumberList = [];

  phoneNumberList = _.union(
    commonHelper.transformObjectToStringList(patientData, 'registeredPhonenumber'),
    commonHelper.transformObjectToStringList(patientData, 'phonenumber')
  );

  return phoneNumberList;
}

const fetchPhonenumberListFromPatientId = async (patientIdList) => {
  const query = {
    attributes: PATIENT_PHONE_ATTRIBUTES
  };
  let initialList,
    patientDataList = [],
    phoneNumberList = [];

  initialList = Array.isArray(patientIdList) ? Array.from(patientIdList) : [patientIdList];
  query.patientId = initialList;

  patientDataList = await patientDataWrapper(query);
  patientDataList.forEach(({ registeredPhonenumber, phonenumber }) => {
    let isValidRegisteredPhonenumber = false,
      isValidPhonenumber = false;

    isValidRegisteredPhonenumber = registeredPhonenumber === phonenumber ||
      (registeredPhonenumber && registeredPhonenumber !== 'NA' && !isNaN(registeredPhonenumber));
    isValidPhonenumber = phonenumber && phonenumber !== 'NA' && !isNaN(phonenumber);

    if (isValidRegisteredPhonenumber) {
      phoneNumberList.push(registeredPhonenumber);
    } else if (isValidPhonenumber) {
      phoneNumberList.push(phonenumber);
    }
  });

  phoneNumberList = _.uniq(phoneNumberList);

  return phoneNumberList;
}

const patientDataWrapper = async (params) => {
  const { patientId, attributes, phonenumber, registeredPhonenumber, phonenumberNotNull,
      registeredPhonenumberNotNull, advertiserId, orderBy, parentIdNotNull, parentIdNull } = params,
    query = {
      where: {},
      raw: true
    };

  typeof attributes === 'string' && _.set(query, 'attributes', [attributes]);
  Array.isArray(attributes) && _.set(query, 'attributes', attributes);

  patientId && _.set(query, 'where.id', queryHelper.generateEqualQueryObject(patientId));
  phonenumber && _.set(query, 'where.phonenumber', queryHelper.generateEqualQueryObject(phonenumber));
  registeredPhonenumber && _.set(query, 'where.registeredPhonenumber',
    queryHelper.generateEqualQueryObject(registeredPhonenumber));
  phonenumberNotNull && _.set(query, 'where.phonenumber', { ..._.get(query, 'where.phonenumber', {}),
    ...queryHelper.generateNotEqualQueryObject(null)});
  registeredPhonenumberNotNull && _.set(query, 'where.registeredPhonenumber',
    { ..._.get(query, 'where.registeredPhonenumber', {}), ...queryHelper.generateNotEqualQueryObject(null)});
  advertiserId && _.set(query, 'where.advertiserId', queryHelper.generateEqualQueryObject(advertiserId));
  typeof parentIdNotNull !== 'undefined' && _.set(query, 'where.parentId',
    queryHelper.generateNotEqualQueryObject(null));
  typeof parentIdNull !== 'undefined' && _.set(query, 'where.parentId', { ..._.get(query, 'where.parentId', {}),
    ...queryHelper.generateEqualQueryObject(null) });

  orderBy && _.set(query, 'order', queryHelper.generateQueryOrderByObject(orderBy));

  return await fetchPatientData(query);
}

const fetchPatientData = async (queryParams) => {
  if (!queryParams) {
    return;
  }

  return await patient.findAll(queryParams);
}

module.exports = {
  filterPatientPhoneNumberList,
  fetchPatientIdFromPhoneNumber,
  patientDataWrapper,
  fetchPhonenumberListFromPatientId,
  fetchPatientData
}