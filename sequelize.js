const config = require('@config'),
  readReplicaConfig = config['readreplicaDB'],
  secondaryDBLoggerConfig = config['secondaryDBLogger'],
  Sequelize = require('sequelize'),
  readReplicaSequelize = new Sequelize(readReplicaConfig.database, readReplicaConfig.username,
    readReplicaConfig.password, readReplicaConfig),
  secondaryDBLoggerSequelize = new Sequelize(secondaryDBLoggerConfig.database, secondaryDBLoggerConfig.username,
    secondaryDBLoggerConfig.password, secondaryDBLoggerConfig);

module.exports = {
  readReplicaSequelize,
  secondaryDBLoggerSequelize
};