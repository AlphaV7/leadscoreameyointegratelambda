"use strict";
const { readReplicaSequelize } = require('@sequelize'),
  Sequelize = require('sequelize');

function modelMapping (sequelize) {
  const receivedPayment = sequelize.define("receivedPayment", {
    paymentStatus : Sequelize.STRING,
    paymentAmount : Sequelize.STRING,
    topic : Sequelize.STRING,
    consultationId : Sequelize.STRING,
    contentId : Sequelize.STRING,
    source : Sequelize.STRING,
    discountedAmount : Sequelize.STRING,
    patientId : Sequelize.STRING,
    transactionId : Sequelize.STRING,
    transactionDate : Sequelize.STRING,
    couponCode : Sequelize.STRING,
    meta : Sequelize.TEXT,
    reason : Sequelize.STRING,
    opscomments : Sequelize.TEXT,
    archive : Sequelize.INTEGER
  });

  return receivedPayment;
}

module.exports = {
  receivedPayment: modelMapping(readReplicaSequelize)
}
