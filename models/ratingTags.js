"use strict";
const { readReplicaSequelize } = require('@sequelize'),
  Sequelize = require('sequelize');

function modelMapping (sequelize) {
  const ratingTags =  sequelize.define('ratingTags', {
    id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    tag: {
      type: Sequelize.TEXT,
      allowNull: false
    }
  }, {
    tableName: 'ratingTags',
    timestamps: true
  });

  ratingTags.associate = models => {
    ratingTags.belongsTo(models.consultRating, {
      foreignKey: 'consultRatingId'
    });
  };

  return ratingTags;
}

module.exports = {
  ratingTags: modelMapping(readReplicaSequelize)
}