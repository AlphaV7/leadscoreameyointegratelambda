"use strict";
const { readReplicaSequelize } = require('@sequelize'),
  Sequelize = require('sequelize');

function modelMapping(sequelize) {
  var LeadAttributeScore = sequelize.define("LeadAttributeScore", {
    patientId : Sequelize.INTEGER,
    attribute: Sequelize.STRING,
    value: Sequelize.STRING,
    score: Sequelize.INTEGER,
    meta : Sequelize.TEXT
  }, {
    freezeTableName: true,
    classMethods: {
      associate: function(models) {

      }
    }
  });

  return LeadAttributeScore;
}

module.exports = {
  leadAttributeScore: modelMapping(readReplicaSequelize)
}