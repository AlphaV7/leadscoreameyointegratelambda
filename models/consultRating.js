"use strict";
const { readReplicaSequelize } = require('@sequelize'),
  Sequelize = require('sequelize');

function modelMapping (sequelize) {
  const consultRating = sequelize.define('consultRating', {
    id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    patientId: {
      type: Sequelize.INTEGER(11),
      allowNull: false
    },
    consultationId: {
      type: Sequelize.INTEGER(11),
      allowNull: false
    },
    appRating: {
      type: Sequelize.INTEGER(1),
      allowNull: false
    },
    doctorRating: {
      type: Sequelize.INTEGER(1),
      allowNull: false
    },
    othersText: {
      type: Sequelize.TEXT,
      allowNull: true,
      defaultValue : null
    }
  }, {
    tableName: 'consultRating',
    timestamps: true
  });

  consultRating.associate = models => {
    consultRating.hasMany(models.ratingTags, {
      foreignKey: 'consultRatingId'
    });
  };

  return consultRating;
}

module.exports = {
  consultRating: modelMapping(readReplicaSequelize)
}