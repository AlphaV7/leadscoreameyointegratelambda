"use strict";
const { readReplicaSequelize } = require('@sequelize'),
  Sequelize = require('sequelize');

function modelMapping(sequelize) {
  var Patient = sequelize.define("Patient", {
    imei:  Sequelize.STRING,
    email: Sequelize.STRING,
    location : Sequelize.TEXT,
    gcm : Sequelize.STRING,
    platform : Sequelize.STRING,
    meta : Sequelize.TEXT,
    version : Sequelize.STRING,
    platform_version : Sequelize.STRING,
    username : Sequelize.STRING,
    password : Sequelize.STRING,
    device : Sequelize.STRING,
    uuid : Sequelize.STRING,
    name : Sequelize.STRING,
    phonenumber : Sequelize.STRING,
    city : Sequelize.STRING,
    registerEmail : Sequelize.STRING,
    validGcm : {type:Sequelize.BOOLEAN, defaultValue :0},
    age : Sequelize.STRING,
    gender : Sequelize.STRING,
    registeredPhonenumber : Sequelize.STRING,
    address : Sequelize.TEXT,
    wallet : Sequelize.DECIMAL,
    source : Sequelize.STRING,
    docsMateDoctor : Sequelize.STRING,
    thyrocare : Sequelize.BOOLEAN,
    bajaj : Sequelize.BOOLEAN,
    priority : Sequelize.STRING,
    appviralityUserKey : Sequelize.STRING,
    advertiserId : Sequelize.STRING,
    simplAllowed : Sequelize.INTEGER,
    adhar : Sequelize.STRING,
    isMedAllUser : Sequelize.INTEGER
  }, {
    classMethods: {
      associate: function(models) {

      }
    }
  });

  return Patient;
}

module.exports = {
  patient: modelMapping(readReplicaSequelize)
}
