module.exports = {
  consults: require('./consults').consults,
  contents: require('./content').contents,
  couponCodes: require('./couponCodes').couponCodes,
  patient: require('./patient').patient,
  customerSupportCallDetails: require('./customerSupportCallDetails').customerSupportCallDetails,
  orders: require('./orders').orders,
  LeadScore: require('./LeadScore').LeadScore,
  corporateUsers: require('./corporateUsers').corporateUsers,
  leadAttributeScore: require('./leadAttributeScore').leadAttributeScore,
  Subscription: require('./Subscription').Subscription,
  receivedPayment: require('./ReceivedPayments').receivedPayment,
  consultRating: require('./consultRating').consultRating,
  ratingTags: require('./ratingTags').ratingTags
}
