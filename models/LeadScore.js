"use strict";
const { readReplicaSequelize } = require('@sequelize'),
  Sequelize = require('sequelize');

function modelMapping(sequelize) {
  var LeadScore = sequelize.define("LeadScore", {
    patientId : Sequelize.INTEGER,
    score: Sequelize.INTEGER,
    version: Sequelize.STRING,
    service: Sequelize.STRING,
    campaignName: Sequelize.STRING,
    meta : Sequelize.TEXT
  }, {
    freezeTableName: true,
    classMethods: {
      associate: function(models) {

      }
    }
  });

  return LeadScore;
}

module.exports = {
  LeadScore: modelMapping(readReplicaSequelize)
}