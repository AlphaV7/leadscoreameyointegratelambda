"use strict";
const { readReplicaSequelize } = require('@sequelize'),
  Sequelize = require('sequelize');

function modelMapping(sequelize) {
  var corporateUsers = sequelize.define("CorporateUsers", {
    source: Sequelize.STRING,
    patientid: Sequelize.INTEGER,
    corporateUserUniqueId: Sequelize.STRING,
    name: Sequelize.STRING,
    phonenumber: Sequelize.STRING,
    age: Sequelize.TINYINT,
    gender: Sequelize.ENUM('Male', 'Female'),
    points: Sequelize.SMALLINT,
    meta: Sequelize.STRING
  }, {
    freezeTableName: true,
    classMethods: {
      associate: function(models) {

      }
    }
  });

  return corporateUsers;
}

module.exports = {
  corporateUsers: modelMapping(readReplicaSequelize)
}