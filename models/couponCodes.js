"use strict";
const { readReplicaSequelize } = require('@sequelize'),
  Sequelize = require('sequelize');

function modelMapping(sequelize) {
  var couponCode = sequelize.define("couponCodes", {
    name: Sequelize.STRING,
    code: Sequelize.STRING,
    discount: Sequelize.STRING,
    reusable: Sequelize.STRING,
    firstOnly : Sequelize.STRING,
    active : Sequelize.STRING,
    topic : Sequelize.STRING,
    invalidTopic : Sequelize.STRING,
    validTill : Sequelize.DATE,
    serviceType : Sequelize.STRING,
    maximum : Sequelize.STRING,
    meta : Sequelize.TEXT
  }, {
    freezeTableName: true,
    classMethods: {
      associate: function(models) {

      }
    }
  });

  return couponCode;
}

module.exports = {
  couponCodes: modelMapping(readReplicaSequelize)
}