"use strict";
const { readReplicaSequelize } = require('@sequelize'),
  Sequelize = require('sequelize');

function modelMapping(sequelize) {
  var Content = sequelize.define("Content", {
    content : Sequelize.TEXT,
    topic : Sequelize.STRING,
    user : Sequelize.STRING,
    domain : Sequelize.STRING,
    status : { type:Sequelize.INTEGER, defaultValue : 0 },
    valid : { type:Sequelize.BOOLEAN, defaultValue : 0 },
    meta : Sequelize.TEXT,
    contentId : Sequelize.STRING,
    patientId : Sequelize.INTEGER,
    age : { type:Sequelize.INTEGER, defaultValue : 0 },
    gender : Sequelize.STRING,
    replied : { type:Sequelize.BOOLEAN, defaultValue : 0 },
    isForwarded:{ type:Sequelize.BOOLEAN, defaultValue : 0 },
    dashboard_archived : { type:Sequelize.BOOLEAN, defaultValue : 0 },
    archive : { type : Sequelize.BOOLEAN, defaultValue : 0 },
    sessionId : Sequelize.TEXT,
    read : { type : Sequelize.BOOLEAN, defaultValue : 0 },
    type : Sequelize.STRING,
    contentMeta : Sequelize.TEXT,
    auto : { type : Sequelize.BOOLEAN, defaultValue : 0 },
    topicbyuser : Sequelize.STRING,
    adminMessageType : Sequelize.STRING,
    consultationId : Sequelize.STRING,
    bot : { type : Sequelize.BOOLEAN, defaultValue : 0 },
    memberId : Sequelize.STRING,
    delivered : { type : Sequelize.BOOLEAN, defaultValue : 0 },
    deliveredAt : Sequelize.DATE,
    localDeliveredAt : Sequelize.DATE,
    readAt : Sequelize.DATE,
    contentLocalTime : Sequelize.BIGINT,
    notificationAt : Sequelize.DATE,
    notification : Sequelize.INTEGER,
    reason : Sequelize.STRING,
    action : Sequelize.STRING,
    timeToSync : Sequelize.BIGINT,
    formSubmitted : Sequelize.BOOLEAN,
    markedDoneDashboard : Sequelize.INTEGER
  }
  , {
    classMethods: {
      associate: function(models) {
        Content.belongsTo(models.Patient, { as:'patient', foreignKey:'patientId' });
        Content.hasMany(models.Forwards, { as : 'forward', foreignKey:'contentId' });
      }
    }
  });

  return Content;
}

module.exports = {
  contents: modelMapping(readReplicaSequelize)
}