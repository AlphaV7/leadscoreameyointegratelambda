"use strict";
const { readReplicaSequelize } = require('@sequelize'),
  Sequelize = require('sequelize');

function modelMapping(sequelize) {
  var orders = sequelize.define("orders", {
    patientId : Sequelize.INTEGER,
    topic : Sequelize.STRING,
    consultationId : Sequelize.INTEGER,
    contentId : Sequelize.INTEGER,
    totalAmount : Sequelize.DECIMAL,
    discountedAmount : Sequelize.DECIMAL,
    walletAmount : Sequelize.DECIMAL,
    netPaidAmount : Sequelize.DECIMAL,
    cashbackAmount : Sequelize.DECIMAL,
    cashbackPercent : Sequelize.DECIMAL,
    couponCode : Sequelize.STRING,
    discountPercent : Sequelize.DECIMAL,
    refunded : Sequelize.INTEGER,
    refundAmount : Sequelize.DECIMAL,
    refundedToWallet : Sequelize.INTEGER,
    type : Sequelize.STRING,
    cartId : Sequelize.INTEGER,
    orderStatus : Sequelize.STRING,
    paymentType: Sequelize.STRING,
    paymentId : Sequelize.INTEGER,
    paymentStatus : Sequelize.STRING,
    transactionId : Sequelize.STRING,
    transactionDate : Sequelize.STRING,
    source : Sequelize.STRING,
    meta : Sequelize.TEXT,
    comments : Sequelize.TEXT
  }, {
    classMethods: {
      associate: function(models) {

      }
    }
  });

  return orders;
}

module.exports = {
  orders: modelMapping(readReplicaSequelize)
}