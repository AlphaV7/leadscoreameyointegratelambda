"use strict";
const { readReplicaSequelize } = require('@sequelize'),
  Sequelize = require('sequelize');

function modelMapping(sequelize) {
  const customerSupportCallDetails = sequelize.define("customerSupportCallDetails", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    lastStatus:Sequelize.TEXT,
    dstPhone:Sequelize.BIGINT,
    campaignId:Sequelize.INTEGER,
    customerCRTId : Sequelize.STRING,
    meta : Sequelize.TEXT,
    ringingTime:Sequelize.INTEGER,
    ivrTime : Sequelize.INTEGER,
    userAssociations: Sequelize.STRING,
    phone : Sequelize.BIGINT,
    dialedTime:Sequelize.DATE,
    customerId: Sequelize.INTEGER,
    srcPhone: Sequelize.STRING,
    recordingFileUrl : Sequelize.TEXT,
    callId : Sequelize.STRING,
    dispositionCode : Sequelize.STRING,
    setupTime :  Sequelize.INTEGER,
    callType : Sequelize.STRING,
    systemDisposition : Sequelize.STRING,
    talkTime : Sequelize.INTEGER,
    callSummary: Sequelize.STRING
  }, {
    classMethods: {
      associate: function(models) {
      }
    }
  });

  return customerSupportCallDetails;
}

module.exports = {
  customerSupportCallDetails: modelMapping(readReplicaSequelize)
}
