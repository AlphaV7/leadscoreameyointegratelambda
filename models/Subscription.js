"use strict";
const { readReplicaSequelize } = require('@sequelize'),
  Sequelize = require('sequelize');

function modelMapping(sequelize) {
  var Subscription = sequelize.define('Subscription', {
    id: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    patientId: {
      type: Sequelize.INTEGER(11)
    },
    orderId: {
      type: Sequelize.INTEGER(11)
    },
    packageId: {
      type: Sequelize.STRING,
      allowNull: false
    },
    validFrom: {
      type: Sequelize.DATE,
      allowNull: false
    },
    validTill: {
      type: Sequelize.DATE,
      allowNull: false
    },
    purchaseType: {
      type: Sequelize.STRING,
      allowNull: true
    },
    status: {
      type: Sequelize.BOOLEAN,
      defaultValue: false
    },
    comment: {
      type: Sequelize.STRING,
      allowNull: true
    },
    relationId: {
      type: Sequelize.STRING,
      allowNull: true
    },
    meta: {
      type: Sequelize.STRING,
      allowNull: true
    }
  },
  {
    tableName: 'Subscription',
    timestamps: true,
    indexes: [
      {
        unique: false,
        fields: ['packageId']
      },
      {
        unique: false,
        fields: ['orderId']
      },
      {
        unique: false,
        fields: ['createdAt']
      },
      {
        unique: false,
        fields: ['validFrom']
      },
      {
        unique: false,
        fields: ['validTill']
      }]
  });

  Subscription.associate = models => {
    Subscription.belongsTo(models.Patient, {
      foreignKey: 'patientId'
    });
  };

  return Subscription;
}

module.exports = {
  Subscription: modelMapping(readReplicaSequelize)
}