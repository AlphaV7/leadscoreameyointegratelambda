const _ = require('lodash'),
  Promise = require('bluebird');

const { ASSOCIATION_ATTRIBUTES } = require('@constants'),
  { patientUtil } = require('@utils'),
  { commonHelper } = require('@helpers');

const removeAssociatedPatientId = async (mainList, deletionList) => {
  const deletionDataParams = {
      attributes: ASSOCIATION_ATTRIBUTES,
      patientId: deletionList,
      registeredPhonenumberNotNull: true
    },
    mainListParams = {
      attributes: ASSOCIATION_ATTRIBUTES,
      patientId: mainList
    };

  let deletionPatientData,
    deletionPatientPhoneNumberList,
    mainPatientPhoneNumberData,
    mainPatientRegisteredPhoneNumberData,
    patientIdList,
    requiredList = [];

  if (!deletionList || !deletionList.length) {
    return mainList;
  }

  deletionPatientData = await patientUtil.patientDataWrapper(deletionDataParams);
  deletionPatientPhoneNumberList = patientUtil.filterPatientPhoneNumberList(deletionPatientData);

  [ mainPatientPhoneNumberData, mainPatientRegisteredPhoneNumberData ] = await Promise.all([
    patientUtil.patientDataWrapper({ ...mainListParams, phonenumber: deletionPatientPhoneNumberList,
      phonenumberNotNull: true }),
    patientUtil.patientDataWrapper({ ...mainListParams, registeredPhonenumber: deletionPatientPhoneNumberList,
      registeredPhonenumberNotNull: true })
  ]);

  patientIdList = _.union(
    commonHelper.transformObjectToNumberList(mainPatientPhoneNumberData, 'id'),
    commonHelper.transformObjectToNumberList(mainPatientRegisteredPhoneNumberData, 'id'),
  );

  requiredList = _.pull(mainList, ..._.union(patientIdList, deletionList));

  return requiredList;
}

const getPatientPhoneNumberMapping = async (patientIdList) => {
  const initialPatientIdList = Array.isArray(patientIdList) ? Array.from(patientIdList) : [patientIdList],
    patientQuery = {
      attributes: ASSOCIATION_ATTRIBUTES,
      patientId: initialPatientIdList
    };
  let patientDataList = [],
    patientIdToNumberMapping = {},
    patientNumberToIdMapping = {};

  patientDataList = await patientUtil.patientDataWrapper(patientQuery);

  patientDataList.forEach(({ id, registeredPhonenumber, phonenumber}) => {
    if (registeredPhonenumber && registeredPhonenumber !== 'NA') {
      patientIdToNumberMapping[id] = registeredPhonenumber;
      patientNumberToIdMapping[registeredPhonenumber] = id;
    } else {
      patientIdToNumberMapping[id] = phonenumber;
      patientNumberToIdMapping[phonenumber] = id;
    }
  });

  return { patientIdToNumberMapping, patientNumberToIdMapping };
}

module.exports = {
  removeAssociatedPatientId,
  getPatientPhoneNumberMapping
}