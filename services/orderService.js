const _ = require('lodash'),
  Promise = require('bluebird');

const { paymentUtil, packageUtil, consultUtil } = require('@utils'),
  { commonHelper } = require('@helpers'),
  { ameyoCampaign } = require('@config'),
  { GOLD_COUPON_PATTERN, NON_PAID_USER_ATTRIBUTES, ORDER_PAYMENT_TYPE, ONE_DAY, CONSULT_TOPIC,
    TOPIC_LIKE } = require('@constants');

const getFailedPaymentPatientList = async () => {
  const params = {
    attributes: ['patientId'],
    couponCodeNotLike: '%HEALTHY%',
    topicNotEqual: [CONSULT_TOPIC.buyMedicine, CONSULT_TOPIC.lifeStyleAdvice, CONSULT_TOPIC.bookLabTest],
    topicNotLike: TOPIC_LIKE.labSelfServe
  };
  let failedPaymentList = [],
    failedPaymentPatientIdList = [];

  failedPaymentList = await paymentUtil.fetchPastFailedPaymentData(params);
  failedPaymentPatientIdList = commonHelper.transformObjectToNumberList(failedPaymentList, 'patientId');
  failedPaymentPatientIdList = _.uniq(failedPaymentPatientIdList);

  return failedPaymentPatientIdList;
}

/**
 * Parse ameyo campaign patientId list to remove non paid gold coupon patientId and map to corresponding
 * campaign
 * @param  {Object} ameyoCampaignPatientData [Object containing patientId list mapped to required campaignName]
 * @return {Object}                          [Data to be posted to Ameyo SQS, mapped to campaignName]
 */
const removeNonPaidGoldCouponUserFromAmeyoCampaigns = async (ameyoCampaignPatientData) => {
  const ameyoSqsCampaignMapping = {};

  await Promise.all(Object.keys(ameyoCampaignPatientData).map(async (campaign) => {
    let currentCampaignName = _.get(ameyoCampaign, `${campaign}.campaignName`),
      currentCampaignMapping = {};

    if (!currentCampaignName || !ameyoCampaignPatientData[campaign].length) {
      return;
    }

    currentCampaignMapping = await filterNonPaidGoldCouponUser(ameyoCampaignPatientData[campaign]);
    _.set(ameyoSqsCampaignMapping, `${currentCampaignName}`, currentCampaignMapping);

    return;
  }));

  return ameyoSqsCampaignMapping;
}

/**
 * Remove non paid gold package user from patientId
 * @param  {[Array, Number]} patientId [patientId in number format]
 * @return {Array}           [List containing patientId in number format]
 */
const filterNonPaidGoldCouponUser = async (patientId) => {
  const patientList = Array.isArray(patientId) ? patientId : [patientId];

  let getNonPaidGoldCouponUserList = [],
    paidGoldCouponUserList = [];

  getNonPaidGoldCouponUserList = await getNonPaidGoldCouponUser(patientList);
  paidGoldCouponUserList = _.pull(patientList, ...getNonPaidGoldCouponUserList);

  return paidGoldCouponUserList;
}

/**
 * Fetch pay later consultation data from orders table. Patient data fetched when paymentType is equal to
 * pay-later consultaiton identifier. Default returns previous day's pay-later data
 * @param  {Object} params [Order-util params]
 * @return {Array}         [Order table data list for pay-later consultation]
 */
const getPayLaterConsultationOrderData = async (params = {}) => {
  const orderParams = {
    paymentType: ORDER_PAYMENT_TYPE.payLaterConsultation,
    createdAtRange: [
      {
        above: new Date().setTime(new Date().getTime() - ONE_DAY),
        below: new Date().setTime(new Date().getTime())
      }
    ],
    ...params
  };
  let payLaterOrderDataList = [];

  payLaterOrderDataList = await paymentUtil.fetchOrderDataWrapper(orderParams);

  return payLaterOrderDataList;
}

/**
 * Removes patientId who started pay-later consultaiton from given list of patientId
 * @param  {[Array,Number]} patientList [List of patientId in number format]
 * @return {[Array]}                    [List of patientId in number format excluding pay-later audience]
 */
const removePayLaterConsultationPatient = async (patientList) => {
  const initialList = Array.isArray(patientList) ? Array.from(patientList) : [patientList],
    orderParams = {
      patientId: initialList
    },
    ratingParams = {
      createdAtRange: { above: new Date().setTime(new Date().getTime() - ONE_DAY) }
    };
  let payLaterOrderDataList = [],
    payLaterPatientIdList = [],
    badAppRatingPatientIdList = [],
    badDoctorRatingPatientIdList = [],
    requiredPatientIdList = [];

  payLaterOrderDataList = await getPayLaterConsultationOrderData(orderParams);
  payLaterPatientIdList = commonHelper.transformObjectToNumberList(payLaterOrderDataList, 'patientId');
  ratingParams.patientId = payLaterPatientIdList;

  [ badAppRatingPatientIdList, badDoctorRatingPatientIdList ] = await Promise.all([
    consultUtil.fetchBadAppRatingData(ratingParams),
    consultUtil.fetchBadDoctorRatingData(ratingParams)
  ]);

  badAppRatingPatientIdList = commonHelper.transformObjectToNumberList(badAppRatingPatientIdList, 'patientId');
  badDoctorRatingPatientIdList = commonHelper.transformObjectToNumberList(badDoctorRatingPatientIdList, 'patientId');

  requiredPatientIdList = _.pull(initialList, ..._.union(badDoctorRatingPatientIdList, badAppRatingPatientIdList));

  return requiredPatientIdList;
}

/**
 * Get patientId list for patient from the given patientId list
 * @param  {Array} patientId [List containing patientId in number format]
 * @return {Array}           [List containing patientId in number format]
 */
const getNonPaidGoldCouponUser = async (patientId) => {
  const orderParams = {
    attributes: NON_PAID_USER_ATTRIBUTES,
    patientId,
    couponCodeLike: `%${GOLD_COUPON_PATTERN}%`
  };

  let nonPaidUserData = [],
    nonPaidUserIdList = [],
    nonPaidCouponCodeList = [],
    nonPaidCouponPatientId = [],
    nonPaidGoldCouponUserList = [];

  if (!patientId) {
    return;
  }

  nonPaidUserData = await paymentUtil.fetchOrderDataWrapper(orderParams);
  nonPaidUserIdList = commonHelper.transformObjectToNumberList(nonPaidUserData, 'patientId');
  nonPaidCouponCodeList = commonHelper.transformObjectToStringList(nonPaidUserData, 'couponCode');
  nonPaidCouponPatientId = packageUtil.getPatientIdFromCouponCode(nonPaidCouponCodeList);

  nonPaidGoldCouponUserList = _.pull(nonPaidUserIdList, ...nonPaidCouponPatientId);

  return nonPaidGoldCouponUserList;
}

module.exports = {
  getPayLaterConsultationOrderData,
  removePayLaterConsultationPatient,
  removeNonPaidGoldCouponUserFromAmeyoCampaigns,
  filterNonPaidGoldCouponUser,
  getNonPaidGoldCouponUser,
  getFailedPaymentPatientList
}