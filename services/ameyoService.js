const _ = require('lodash'),
  Promise = require('bluebird');

const { patientUtil, customerSupportCallDetailsUtil, ameyoUtil, awsUtil } = require('@utils'),
  patientService = require('./patientService'),
  { commonHelper, logHelper } = require('@helpers'),
  { ONE_DAY, PACKAGE_SALE_CAMPAIGN_ID, REQ_DISPOSITION_ATTRIBUTES,
    CALL_NOT_CONNECTED_SYSTEM_DISPOSITION, CALL_CONNECTED_SYSTEM_DISPOSITION, CALL_NOT_CONNECTED_CALL_TYPE,
    CALL_CONNECTED_CALL_TYPE, LEAD_SCORE_PATIENT_ATTRIBUTE, FOLLOW_UP_QUERY,
    AMEYO_COMMAND_NAME } = require('@constants');

const getCallNotConnectedPatientList = async (params) => {
  const createdAtRange = [
      {
        above: new Date().setTime(new Date().getTime() - ONE_DAY),
        below: new Date().setTime(new Date().getTime())
      }
    ],
    patientParams = {
      registeredPhonenumber: true
    },
    patientIdMapping = {},
    requiredPatientIdList = [];

  await Promise.all(createdAtRange.map(async (range, index) => {
    const currentDispositionParams = {
      attributes: REQ_DISPOSITION_ATTRIBUTES,
      createdAtRange: range
    };

    let notConnectedDispositionData = [],
      connectedDispositionData = [],
      notConnectedPhoneNumberList = [],
      connectedPhoneNumberList = [],
      requiredNotConnectedPhoneNumberList = [],
      requiredDispositionPhoneNumberList,
      patientIdList = [];

    [ notConnectedDispositionData, connectedDispositionData ] = await Promise.all([
      getCallNotConnectedStatusData({ ...currentDispositionParams, callType: CALL_NOT_CONNECTED_CALL_TYPE }),
      getCallConnectedStatusData({ ...currentDispositionParams, callType: CALL_CONNECTED_CALL_TYPE })
    ]);

    notConnectedPhoneNumberList = commonHelper.transformObjectToNumberList(notConnectedDispositionData, 'phone');
    connectedPhoneNumberList = commonHelper.transformObjectToNumberList(connectedDispositionData, 'phone');

    requiredNotConnectedPhoneNumberList = _.uniq(_.pull(notConnectedPhoneNumberList, ...connectedPhoneNumberList));
    requiredDispositionPhoneNumberList = commonHelper.transformObjectToStringList(commonHelper.filterByCount(
      notConnectedDispositionData, { phone: requiredNotConnectedPhoneNumberList }, 3), 'phone');

    patientIdList = await patientUtil.fetchPatientIdFromPhoneNumber(requiredDispositionPhoneNumberList, patientParams);

    _.set(patientIdMapping, `${index}`, patientIdList);
  }));

  Object.keys(patientIdMapping).forEach((index) => {
    requiredPatientIdList.push(...patientIdMapping[index]);
  });

  return requiredPatientIdList;
}

const getCallNotConnectedStatusData = async (params) => {
  const dispositionParams = {
    attributes: REQ_DISPOSITION_ATTRIBUTES,
    campaignId: PACKAGE_SALE_CAMPAIGN_ID,
    systemDisposition: CALL_NOT_CONNECTED_SYSTEM_DISPOSITION,
    ...params
  };

  let callNotConnectedPatientData = [];

  callNotConnectedPatientData = await customerSupportCallDetailsUtil.customerSupportCallDetailsWrapper(
    dispositionParams);

  return callNotConnectedPatientData;
}

const removeThreeDayConnectedCall = async (patientIdList) => {
  const query = {
    createdAtRange: [
      {
        above: new Date().setTime(new Date().getTime() - 3*ONE_DAY),
        below: new Date().setTime(new Date().getTime())
      }
    ],
    callType: CALL_CONNECTED_CALL_TYPE
  };
  let initialPatientIdList = Array.from(patientIdList),
    phoneNumberList = [],
    connectedCallData = [],
    connectedCallPhoneNumberList = [],
    notConnectedPhoneNumberList = [],
    notConnectedPatientIdList = [],
    { patientNumberToIdMapping } = await patientService.getPatientPhoneNumberMapping(initialPatientIdList);

  phoneNumberList = Object.keys(patientNumberToIdMapping);
  connectedCallData = await getCallConnectedStatusData(query);
  connectedCallPhoneNumberList = commonHelper.transformObjectToStringList(connectedCallData, 'phone');
  notConnectedPhoneNumberList = _.pull(phoneNumberList, ...connectedCallPhoneNumberList);

  notConnectedPhoneNumberList.forEach((phonenumber) => {
    notConnectedPatientIdList.push(patientNumberToIdMapping[phonenumber]);
  });

  return notConnectedPatientIdList;
}

const getCallConnectedStatusData = async (params) => {
  const dispositionParams = {
    attributes: REQ_DISPOSITION_ATTRIBUTES,
    campaignId: PACKAGE_SALE_CAMPAIGN_ID,
    systemDisposition: CALL_CONNECTED_SYSTEM_DISPOSITION,
    ...params
  };

  let callConnectedPatientData = [];

  callConnectedPatientData = await customerSupportCallDetailsUtil.customerSupportCallDetailsWrapper(dispositionParams);

  return callConnectedPatientData;
}

const getDispositionPatientList = async (params) => {
  let dispositionDataList = [],
    phoneNumberList = [],
    patientIdList = [];

  dispositionDataList = await customerSupportCallDetailsUtil.fetchCustomerSupportCallDetailRawQuery(FOLLOW_UP_QUERY.v4);
  phoneNumberList = commonHelper.transformObjectToStringList(dispositionDataList, 'phone');
  patientIdList = await patientUtil.fetchPatientIdFromPhoneNumber(phoneNumberList);

  return patientIdList;
}

const postCampaignLeadListToAmeyo = async (packageSalePaidLeadList, packageSaleUnPaidLeadList, followUpLeadList,
  notConnectedLeadList, shortDurationConnectedScoreList, failedPaymentLeadList, recentPaidLeadList,
  pastPaidLeadList) => {

  let packageSalePaidCampaignName,
    packageSaleUnPaidCampaignName,
    followUpCampaignName,
    notConnectedCampaignName,
    shortDurationConnectedCampaignName,
    failedPaymentCampaignName,
    recentPaidCampaignName,
    pastPaidCampaignName,
    leadScoreDataList = [];

  packageSalePaidCampaignName = ameyoUtil.getAmeyoLeadScoreV2CampaignKey();
  packageSaleUnPaidCampaignName = ameyoUtil.getAmeyoUnpaidConsultCampaignKey();
  followUpCampaignName = ameyoUtil.getAmeyoDispositionCampaignKey();
  notConnectedCampaignName = ameyoUtil.getAmeyoDispositionNotConnectedCampaignKey();
  shortDurationConnectedCampaignName = ameyoUtil.getAmeyoShortDurationConnectedCampaignKey();
  failedPaymentCampaignName = ameyoUtil.getAmeyoFailedPaymentCampaignKey();
  recentPaidCampaignName = ameyoUtil.getAmeyoRecentPaidCampaignKey();
  pastPaidCampaignName = ameyoUtil.getAmeyoPastPaidCampaignKey();

  leadScoreDataList = [
    { leadScoreList: packageSalePaidLeadList, campaignName: packageSalePaidCampaignName },
    { leadScoreList: packageSaleUnPaidLeadList, campaignName: packageSaleUnPaidCampaignName },
    { leadScoreList: followUpLeadList, campaignName: followUpCampaignName },
    { leadScoreList: notConnectedLeadList, campaignName: notConnectedCampaignName },
    { leadScoreList: shortDurationConnectedScoreList, campaignName: shortDurationConnectedCampaignName },
    { leadScoreList: failedPaymentLeadList, campaignName: failedPaymentCampaignName },
    { leadScoreList: recentPaidLeadList, campaignName: recentPaidCampaignName },
    { leadScoreList: pastPaidLeadList, campaignName: pastPaidCampaignName }
  ];

  await Promise.all(leadScoreDataList.map((async ({ leadScoreList, campaignName }) => {
    let patientIdList = [],
      phoneNumberList = [];

    if (!leadScoreList || !leadScoreList.length) {
      return;
    }

    patientIdList = commonHelper.transformObjectToNumberList(leadScoreList, LEAD_SCORE_PATIENT_ATTRIBUTE);
    phoneNumberList = await patientUtil.fetchPhonenumberListFromPatientId(patientIdList);

    await postCampaignPhoneNumberAmeyoSQS(campaignName, phoneNumberList);
  })));

  return;
}

const postVernacularLeadListToAmeyo = async (vernacularLeadData) => {
  let locationList = Object.keys(vernacularLeadData),
    campaignName,
    leadScoreList = [],
    patientIdList = [],
    phoneNumberList = [];

  for (let location of locationList) {
    leadScoreList = vernacularLeadData[location];
    campaignName = ameyoUtil.getAmeyoVernacularCampaignKey(location);
    patientIdList = commonHelper.transformObjectToNumberList(leadScoreList, LEAD_SCORE_PATIENT_ATTRIBUTE);
    phoneNumberList = await patientUtil.fetchPhonenumberListFromPatientId(patientIdList);

    await postCampaignPhoneNumberAmeyoSQS(campaignName, phoneNumberList);
  }

  return;
}

/**
 * Maps patientIds to phone number and generate AmeyoSqs object associated with campaign
 * @param  {String} campaign  [Name of campaign to which data has to be posted]
 * @param  {Array}  patientId [List of patientId]
 */
const postCampaignPhoneNumberAmeyoSQS = async (campaignName, phoneNumberList) => {
  let ameyoSqsCampaignData = {};

  ameyoSqsCampaignData = {
    commandName: AMEYO_COMMAND_NAME.uploadContacts,
    campaignName: campaignName,
    customerRecords: []
  };

  phoneNumberList.forEach((phonenumber) => (
    ameyoSqsCampaignData.customerRecords.push({phone1: phonenumber, name: phonenumber})
  ));

  await postToAmeyoSQS(ameyoSqsCampaignData);

  return;
}

const postToAmeyoSQS = async (data) => {
  const awsConfig = awsUtil.getAwsConfig(),
    sqsParams = {
      MessageBody: JSON.stringify(data),
      QueueUrl: awsConfig.badRatingsNPSAmeyoPost,
      DelaySeconds: 0
    };

  logHelper.log('Posting data to ameyo sqs: ', JSON.stringify(data));
  logHelper.log('Count: ', data.customerRecords.length);

  return await awsUtil.postParamsToSQS(sqsParams);
}

module.exports = {
  removeThreeDayConnectedCall,
  getCallConnectedStatusData,
  getCallNotConnectedStatusData,
  getCallNotConnectedPatientList,
  getDispositionPatientList,
  postCampaignLeadListToAmeyo,
  postCampaignPhoneNumberAmeyoSQS,
  postVernacularLeadListToAmeyo,
  postToAmeyoSQS
}