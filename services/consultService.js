const _ = require('lodash'),
  Promise = require('bluebird');

const { commonHelper } = require('@helpers'),
  { consultUtil, summarizedConsultsUtil, contentUtil, paymentUtil } = require('@utils'),
  { EXCLUDE_TOPICS, ONE_DAY, CONTENT_ADMIN_TYPE, CONTENT_TYPE, CONSULT_GENDER_TYPE,
    ONE_MONTH } = require('@constants');

const CONSULT_DEPT_SIZE = 75;

/**
 * Get patientId associated with summarized and closed consultataion for past 1 DAY
 * summarized consultation excludes patientId with bad NPS rating
 * @return {Object} [patientId list corresponding to consultation]
 */
const getPaidConsultationPatientData = async () => {
  let summarizedConsultPatientList = [],
    askQueryConsultPatientList = [],
    nonDoctorConsultPatientList = [],
    askQueryRepeatPatientList = [],
    consultataionPatientData = {};

  [ summarizedConsultPatientList, askQueryConsultPatientList, nonDoctorConsultPatientList ] = await Promise.all([
    getSummarizedConsultPatientList(),
    getAskQueryConsultPatientList(),
    getNonDoctorConsultationPatientList()
  ]);

  _.pull(askQueryConsultPatientList, ...summarizedConsultPatientList);
  askQueryRepeatPatientList = await filterRepeatConsultPatientList(askQueryConsultPatientList);

  consultataionPatientData = {
    nonDoctorConsult: nonDoctorConsultPatientList,
    summarized: summarizedConsultPatientList,
    paid: askQueryRepeatPatientList
  };

  return consultataionPatientData;
}

const getUnpaidConsultPatientList = async () => {
  const askQueryParams = {
    attributes: ['patient', 'topic'],
    gender: CONSULT_GENDER_TYPE.male
  };

  let summarizedConsultPatientList = [],
    askQueryConsultData = [],
    askQueryConsultPatientList = [],
    repeatConsultPatientList = [],
    nonRepeastConsultPatientList = [],
    unpaidConsultPatientList = [];

  [ askQueryConsultData, summarizedConsultPatientList ] = await Promise.all([
    getAskQueryConsultPatientData(askQueryParams),
    getSummarizedConsultPatientList()
  ]);

  askQueryConsultData = segmentDepartmentDistribution(askQueryConsultData);
  askQueryConsultPatientList = commonHelper.transformObjectToNumberList(askQueryConsultData, 'patient');
  _.pull(askQueryConsultPatientList, ...summarizedConsultPatientList);

  repeatConsultPatientList = await filterRepeatConsultPatientList(askQueryConsultPatientList);
  nonRepeastConsultPatientList = _.pull(askQueryConsultPatientList, ...repeatConsultPatientList);
  unpaidConsultPatientList = await filterPaymentOptionReceivedPatientList(nonRepeastConsultPatientList);
  unpaidConsultPatientList = _.uniq(unpaidConsultPatientList);

  return unpaidConsultPatientList;
}

/**
 * List of summarized doctor consults that happened in past 1 Day
 * @return {Array} [Array containing list of patientIds in number format]
 */
const getSummarizedConsultPatientList = async () => {
  const summarizedConsultParams = {
      summarizedAfter: new Date().setTime(new Date().getTime() - ONE_DAY),
      excludeTopics: EXCLUDE_TOPICS
    },
    summarizedConsultPatientData =
      await summarizedConsultsUtil.fetchSummarizedConsultPatientList(summarizedConsultParams);

  return commonHelper.transformObjectToNumberList(summarizedConsultPatientData, 'patientId');
}

/**
 * Filters out patientId from giving list that have taken at least 1 consultation before
 * @return {Array} [Array containing list of patientIds in number format]
 */
const filterRepeatConsultPatientList = async (patientIdList) => {
  const params = {
    attributes: ['patient'],
    patientId: patientIdList,
    acceptedConsultationNotNull: true,
    valid: true,
    excludeTopics: EXCLUDE_TOPICS,
    summarizedAtRange: {
      below: new Date().setTime(new Date().getTime() - ONE_DAY)
    }
  };
  let repeatConsultPatientData = {},
    repeatConsultPatientList = [];

  if (!patientIdList || !patientIdList.length) {
    return [];
  }

  repeatConsultPatientData = await consultUtil.fetchConsultsWrapper(params);
  repeatConsultPatientList = commonHelper.transformObjectToNumberList(repeatConsultPatientData, 'patient');
  repeatConsultPatientList = _.uniq(repeatConsultPatientList);

  return repeatConsultPatientList;
}

const filterPaymentOptionReceivedPatientList = async (patientIdList) => {
  const params = {
    attributes: ['patientId'],
    patientId: patientIdList,
    adminMessageType: CONTENT_ADMIN_TYPE.paymentOptionReceived,
    type: CONTENT_TYPE.dynamicMessage,
    read: true,
    delivered: true,
    createdAtRange: {
      above: new Date().setTime(new Date().getTime() - ONE_DAY)
    }
  };
  let contentPatientData = {},
    contentPatientList = [];

  if (!patientIdList || !patientIdList.length) {
    return [];
  }

  contentPatientData = await contentUtil.fetchContentData(params);
  contentPatientList = commonHelper.transformObjectToNumberList(contentPatientData, 'patientId');
  contentPatientList = _.uniq(contentPatientList);

  return contentPatientList;
}

/**
 * List of AskQuery consultation patiens that happened in past 1 Day
 * @return {Array} [Array containing list of patientIds in number format]
 */
const getAskQueryConsultPatientList = async (params) => {
  let askQueryConsultPatientData = {},
    askQueryConsultPatientList = [];

  askQueryConsultPatientData = await getAskQueryConsultPatientData(params);
  askQueryConsultPatientList = commonHelper.transformObjectToNumberList(askQueryConsultPatientData, 'patient');
  askQueryConsultPatientList = _.uniq(askQueryConsultPatientList);

  return askQueryConsultPatientList;
}

/**
 * List of AskQuery consultation patient's data that happened in past 1 Day
 * @return {Array} [Array containing list of consultation data from Consults table]
 */
const getAskQueryConsultPatientData = async (queryParams = {}) => {
  const params = {
    status: null,
    acceptedConsultation: null,
    attributes: ['patient'],
    createdAfter: new Date().setTime(new Date().getTime() - ONE_DAY),
    excludeTopics: EXCLUDE_TOPICS,
    ...queryParams
  };
  let askQueryConsultPatientData = {};

  askQueryConsultPatientData = await consultUtil.fetchConsultsWrapper(params);

  return askQueryConsultPatientData;
}

/**
 * Fetch and return equally distributed consultation data wrt consultation topic
 * @param  {Array} consultDataList [Array containing list of consultation data from Consults table]
 * @return {Array} [Array containing list of consultation data from Consults table]
 */
const segmentDepartmentDistribution = (consultDataList) => {
  const consultDepartmentMap = {},
    finalConsultDataList = [];
  let currentDepartmentConsultList = [];

  consultDataList.forEach(({ topic }, index) => {
    !consultDepartmentMap[topic] && _.set(consultDepartmentMap, `${topic}`, []);
    consultDepartmentMap[topic].push(consultDataList[index]);
  });

  Object.keys(consultDepartmentMap).forEach((department) => {
    currentDepartmentConsultList = consultDepartmentMap[department].slice(0, CONSULT_DEPT_SIZE);

    finalConsultDataList.push(...currentDepartmentConsultList);
  });

  return finalConsultDataList
}

/**
 * Fetch patientId list of patients purchasing consultations without doctor intervention (Diet charts,
 *   Life style charts, etc)
 * @return {Array} [Array containing list of patientIds in number format]
 */
const getNonDoctorConsultationPatientList = async () => {
  const orderParams = {
    attributes: ['patientId'],
    createdAtRange: {
      above: new Date().setTime(new Date().getTime() - ONE_DAY)
    }
  };
  let dietChartDataList = [],
    lifeStyleAdviceDataList = [],
    patientIdList = [];

  [ dietChartDataList, lifeStyleAdviceDataList ] = await Promise.all([
    paymentUtil.fetchDietChartData(orderParams),
    paymentUtil.fetchLifeStyleAdviceData(orderParams)
  ]);

  patientIdList = _.union(
    commonHelper.transformObjectToNumberList(dietChartDataList, 'patientId'),
    commonHelper.transformObjectToNumberList(lifeStyleAdviceDataList, 'patientId')
  );

  return patientIdList;
}

const fetchRecentPaidPatientList = async (params = {}) => {
  const recentPaymentQuery = {
    refunded: false,
    createdAtRange: {
      above: new Date().setTime(new Date().getTime() - 5*ONE_DAY)
    },
    ...params
  };

  let paymentDataList = {},
    patientIdList = [];

  paymentDataList = await paymentUtil.fetchOrderDataWrapper(recentPaymentQuery);
  patientIdList = commonHelper.transformObjectToNumberList(paymentDataList, 'patientId');
  patientIdList = _.uniq(patientIdList);

  return patientIdList;
}

const fetchPastPaidPatientList = async (params = {}) => {
  const pastPaymentQuery = {
    refunded: false,
    createdAtRange: {
      below: new Date().setTime(new Date().getTime() - 6*ONE_DAY),
      above: new Date().setTime(new Date().getTime() - 12*ONE_MONTH)
    },
    ...params
  };

  let paymentDataList = {},
    patientIdList = [];

  paymentDataList = await paymentUtil.fetchOrderDataWrapper(pastPaymentQuery);
  patientIdList = commonHelper.transformObjectToNumberList(paymentDataList, 'patientId');
  patientIdList = _.uniq(patientIdList);

  return patientIdList;
}

const fetchLeadDataByRecency = async () => {
  let summarizedConsultPatientList = [],
    askQueryConsultPatientList = [],
    nonDoctorConsultPatientList = [],
    recencyPatientData = {},
    recentPaymentPatientList = [],
    pastPaymentPatientList = [];

  [ summarizedConsultPatientList, askQueryConsultPatientList, nonDoctorConsultPatientList ] = await Promise.all([
    getSummarizedConsultPatientList(),
    getAskQueryConsultPatientList(),
    getNonDoctorConsultationPatientList()
  ]);

  _.pull(askQueryConsultPatientList, ...summarizedConsultPatientList);

  [ recentPaymentPatientList, pastPaymentPatientList ] = await Promise.all([
    fetchRecentPaidPatientList({ patientId: askQueryConsultPatientList }),
    fetchPastPaidPatientList({ patientId: askQueryConsultPatientList })
  ]);

  recentPaymentPatientList = _.uniq([ ...recentPaymentPatientList, ...summarizedConsultPatientList ]);
  pastPaymentPatientList = _.uniq([ ...pastPaymentPatientList, nonDoctorConsultPatientList ]);

  recencyPatientData = {
    recent: recentPaymentPatientList,
    nonRecent: pastPaymentPatientList
  };

  return recencyPatientData;
}

module.exports = {
  fetchLeadDataByRecency,
  filterPaymentOptionReceivedPatientList,
  filterRepeatConsultPatientList,
  getAskQueryConsultPatientList,
  getNonDoctorConsultationPatientList,
  getPaidConsultationPatientData,
  getSummarizedConsultPatientList,
  getUnpaidConsultPatientList,
  segmentDepartmentDistribution
}