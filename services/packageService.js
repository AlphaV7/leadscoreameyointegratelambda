const _ = require('lodash');

const { REMOVE_COUPON_CODE, REQ_ORDER_ATTRIBUTES, ONE_DAY, GOLD_PACKAGE_ID,
    ORDER_PAYMENT_SUCCESS_STATUS, ORDER_PACKAGE_NAME_LIST, ORDER_GOLD_PACKAGE_TYPE,
    GOLD_COUPON_PATTERN } = require('@constants'),
  { paymentUtil, corporateUsersUtil, subscriptionUtil } = require('@utils'),
  { commonHelper } = require('@helpers');

const removeGoldSubscriptionPatient = async (patientIdList) => {
  let patientListClone = [],
    subscriptionDataList = [],
    isValidSubscription = [];

  patientListClone = Array.from(patientIdList);
  subscriptionDataList = await subscriptionUtil.fetchRecentSubscriptionData(patientIdList);

  subscriptionDataList.forEach(({ patientId, packageId, validTill }) => {
    if (!validTill || new Date(validTill).getTime() <= new Date().getTime()) {
      return;
    }

    isValidSubscription = (
      GOLD_PACKAGE_ID.annual.includes(packageId)
      || (
        GOLD_PACKAGE_ID.starter.includes(packageId)
        && new Date(validTill) >= new Date().setTime(new Date().getTime() + 5*ONE_DAY)
      )
    );

    isValidSubscription && _.pull(patientListClone, patientId);
  });

  return patientListClone;
}

/**
 * Removes patient that successfully purchased gold package. Checks order table to fetch
 * required patientId list
 * @param  {Array} patientList [List of patient Id]
 * @param  {Object} params     [params for order table's query wrapper]
 * @return {Array}             [List of patient Id]
 */
const removeGoldPackagePurchasePatient = async (patientList, params = {}) => {
  const orderParams = {
    patientId: patientList,
    paymentStatus: ORDER_PAYMENT_SUCCESS_STATUS,
    refunded: false,
    $or: [
      {
        packageId: { $in: GOLD_PACKAGE_ID.annual }
      },
      {
        packageName: { $in: ORDER_PACKAGE_NAME_LIST }
      },
      {
        packageType: { $in: ORDER_GOLD_PACKAGE_TYPE }
      },
      {
        couponCode: { $like: `%${GOLD_COUPON_PATTERN}%`}
      }
    ],
    ...params
  };

  let orderDataList = [],
    patientListClone = [],
    orderDataPatientIdList = [],
    nonPackagePurchasePatientList = [];

  patientListClone = Array.from(patientList);
  orderDataList = await paymentUtil.fetchOrderDataWrapper(orderParams);
  orderDataPatientIdList = commonHelper.transformObjectToNumberList(orderDataList, 'patientId');
  nonPackagePurchasePatientList = _.pull(patientListClone, ...orderDataPatientIdList);

  return nonPackagePurchasePatientList;
}

/**
 * Removes patient that successfully purchased gold package and have HEALTHY code associated with
 * their patientId.
 * @param  {Array} patientList           [List of patient Id]
 * @param  {Object} couponCodeParams     [params for couponCodes table's query wrapper]
 * @param  {Object} orderParams          [params for order table's query wrapper]
 * @return {Array}                       [List of patient Id]
 */
const removeGoldPackagePatient = async (patientIdList, couponCodeParams, orderParams) => {
  let nonSubscriptionPatientList,
    nonPackagePurchasePatientList;

  nonSubscriptionPatientList = await removeGoldSubscriptionPatient(patientIdList, couponCodeParams);
  nonPackagePurchasePatientList = await removeGoldPackagePurchasePatient(nonSubscriptionPatientList, couponCodeParams);

  return nonPackagePurchasePatientList;
}

/**
 * Remove corporate user patientId. Fetch patientId from corporateUsers table and filter out
 * non-corporate patientId
 * @param  {[type]} patientList [List of patient Ids]
 * @param  {Object} params      [params for corporateUsers table's query wrapper]
 * @return {[type]}             [List of non-corporate patientIds]
 */
const removeCorporatePatient = async (patientList, params) => {
  const patientListClone = Array.from(patientList),
    query = {
      attributes: ['patientid'],
      patientIdNotEqual: 0
    };
  let corporateDataList = [],
    corporatePatientList = [],
    nonCorporatePatientList = [];

  corporateDataList = await corporateUsersUtil.fetchCorporateUsersWrapper(query);
  corporatePatientList = commonHelper.transformObjectToNumberList(corporateDataList, 'patientid');
  nonCorporatePatientList = _.pull(patientListClone, ...corporatePatientList);

  return nonCorporatePatientList;
}

/**
 * Filter out patient associated with usage of coupon code. Currently removes OLA and Swiggy coupon users
 * @param  {Array} patientId [List of patient Ids]
 * @param  {Object} params   [params for orders table's query wrapper]
 * @return {Array}           [List of patient Ids that haven't used coupon]
 */
const removeCouponCodePatientList = async (patientList, params = {}) => {
  const orderParams = {
    attributes: REQ_ORDER_ATTRIBUTES,
    patientId: patientList,
    couponCodeEqual: REMOVE_COUPON_CODE,
    ...params
  };

  let patientListClone,
    couponCodePatientData,
    couponCodePatintList,
    requiredPatientList = [];

  patientListClone = Array.from(patientList);
  couponCodePatientData = await paymentUtil.fetchOrderDataWrapper(orderParams);
  couponCodePatintList = commonHelper.transformObjectToNumberList(couponCodePatientData, 'patientId');

  requiredPatientList = _.pull(patientListClone, ...couponCodePatintList);

  return requiredPatientList;
}

module.exports = {
  removeGoldPackagePatient,
  removeGoldPackagePurchasePatient,
  removeCouponCodePatientList,
  removeCorporatePatient
}