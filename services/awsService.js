const _ = require('lodash');

const { awsUtil } = require('@utils'),
  { logHelper } = require('@helpers')

const STRING_HASH_PREFIX_LENGTH = 62,
  CAMPAIGN_IDENTIFIER = {
    experiment: 'packageLeadExperimentCampaign',
    normal: 'packageLeadNormalCampaign',
    followup: 'dispositionLeadCampaign'
  };

const fetchPatientScoreLogs = async (params) => {
  let allLogs,
    dateToScoreListMapping = {};

  allLogs = await fetchLeadScoreLambdaLogs(params);

  Object.keys(allLogs).forEach((timeStamp) => {
    const { events } = allLogs[timeStamp];
    let isExperimentCampaign,
      isNormalCampaign,
      isFollowUpCampaign,
      campaignIdenifier,
      objectWritePath;

    events.forEach((eventElement) => {
      let { message } = eventElement,
        jsonObject;

      message = message.substr(STRING_HASH_PREFIX_LENGTH).replace('\n', '');

      try {
        jsonObject = JSON.parse(message);
      } catch (err) {
        logHelper.log('Error: ', err);
      }

      if (jsonObject && Array.isArray(jsonObject) && jsonObject[0].patientId) {
        campaignIdenifier = isExperimentCampaign ? 'experiment' : (isNormalCampaign ? 'normal' :
          (isFollowUpCampaign ? 'followup' : 'default'));
        objectWritePath = `${new Date(timeStamp).getTime()}.${CAMPAIGN_IDENTIFIER[campaignIdenifier]}`;

        _.set(dateToScoreListMapping, objectWritePath, jsonObject);
      }

      isExperimentCampaign = message === CAMPAIGN_IDENTIFIER.experiment;
      isNormalCampaign = message === CAMPAIGN_IDENTIFIER.normal;
      isFollowUpCampaign = message === CAMPAIGN_IDENTIFIER.followup;
    });
  });

  return dateToScoreListMapping;
}

const fetchLeadScoreLambdaLogs = async (params) => {
  const query = {
    logGroupName: `leadScoreAmeyoIntegrate`,
    ...params
  };

  return await awsUtil.fetchCloudWatchLogs(query);
}

module.exports = {
  fetchPatientScoreLogs,
  fetchLeadScoreLambdaLogs
}