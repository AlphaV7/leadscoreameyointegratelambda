const _ = require('lodash');

const { commonHelper, logHelper } = require('@helpers'),
  patientService = require('./patientService'),
  { EXOTEL_API, EXOTEL_XML_RESPONSE_PATH } = require('@constants'),
  { fetchDataUtil } = require('@utils'),
  bugsnag = require('@bugsnag');

const getPatientVernacularMapping = async (patientIdList) => {
  const initialPatientList = Array.isArray(patientIdList) ? Array.from(patientIdList): [patientIdList];
  let patientIdToLocationMapping = {},
    phoneNumberList = [],
    phoneNumberLocationMapList = [];

  let { patientNumberToIdMapping } = await patientService.getPatientPhoneNumberMapping(initialPatientList);

  phoneNumberList = Object.keys(patientNumberToIdMapping);
  phoneNumberLocationMapList = await getPhonenumberLocationMapping(phoneNumberList);

  Object.keys(phoneNumberLocationMapList).forEach((phonenumber) => {
    patientIdToLocationMapping[patientNumberToIdMapping[phonenumber]] = phoneNumberLocationMapList[phonenumber];
  });

  return patientIdToLocationMapping;
}

const getPhonenumberLocationMapping = async (phoneNumberList) => {
  const phoneNumberToLocationMapping = {};
  let currentLocation;

  for (let number of phoneNumberList) {
    currentLocation = await getPhoneNumberLocation(number);
    phoneNumberToLocationMapping[number] = currentLocation;
  }

  return phoneNumberToLocationMapping;
}

const getPhoneNumberLocation = async (phonenumber) => {
  const requestOptions = {
    method: 'GET',
    url: `${EXOTEL_API.location}${phonenumber}`
  };
  let responseBody;

  try {
    responseBody = await fetchDataUtil.fetchData(requestOptions);
  } catch (err) {
    logHelper.log('Error: ', err);
    bugsnag.notify(err, { metaData: phonenumber });
  }

  responseBody = await commonHelper.parseXmlString(responseBody);

  return _.get(responseBody, EXOTEL_XML_RESPONSE_PATH.state);
}

const mapLocationDataToLeadScoreList = async (leadScoreList, patientIdToLocationMapping) => {
  let patientIdList = [];

  if (!patientIdToLocationMapping) {
    patientIdList = commonHelper.transformObjectToNumberList(leadScoreList, 'patientId');
    patientIdToLocationMapping = await getPatientVernacularMapping(patientIdList);
  }

  leadScoreList.forEach(({ patientId }, index) => {
    leadScoreList[index].location = patientIdToLocationMapping[patientId];
  });

  return leadScoreList;
}

module.exports = {
  getPatientVernacularMapping,
  mapLocationDataToLeadScoreList,
  getPhoneNumberLocation
}