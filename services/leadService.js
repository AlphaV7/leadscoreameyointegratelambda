const _ = require('lodash');

const { orderService, ameyoService, patientService, packageService } = require('@services-internal'),
  { paymentUtil, scoreCardUtil, consultUtil } = require('@utils'),
  { commonHelper } = require('@helpers'),
  { LEAD_SCORE_TABLE_ATTRIBUTES } = require('@constants');

const BUCKET_SIZE = 100,
  BUCKET_SCORE_INTERVAL = 20;

const segmentScoreList = (leadScoreList) => {
  const scoreBucket = {},
    finalScoreList = [];
  let currentBucketIndex,
    currentRequiredLeadList = [];

  leadScoreList.forEach(({ score }, index) => {
    currentBucketIndex = parseInt(score/BUCKET_SCORE_INTERVAL);

    !scoreBucket[currentBucketIndex] && _.set(scoreBucket, `${currentBucketIndex}`, []);
    scoreBucket[currentBucketIndex].push(leadScoreList[index]);
  });

  Object.keys(scoreBucket).forEach((bucketIdentifier) => {
    currentRequiredLeadList = scoreBucket[bucketIdentifier].slice(0, BUCKET_SIZE);

    finalScoreList.push(...currentRequiredLeadList);
  });

  return finalScoreList;
}

/**
 * Fetch list of patientId mapped to score card V2 campaign key.
 * @param  {Array} summarizedPatientList [List of patientId with summarized consultation]
 * @param  {Array} unpaidPatientList     [List of patientId with unpaid consultation]
 * @return {Object}                      [Campaign key mapped to list of patientId]
 */
const getCampaignLeadScoreList = async (patientIdList, params = {}) => {
  const logLeadParams = _.pick(params, LEAD_SCORE_TABLE_ATTRIBUTES) || {};
  let patientIdListClone = [],
    patientScoreMapping = {},
    campaignLeadScoreList = [];

  if (!patientIdList || !patientIdList.length) {
    return [];
  }

  patientIdListClone = Array.from(_.uniq(patientIdList));

  patientScoreMapping = await calculatePatientScore(patientIdListClone);
  patientIdListClone.forEach((patientId) => {
    campaignLeadScoreList.push({ ...patientScoreMapping[patientId], ...logLeadParams});
  });

  return campaignLeadScoreList;
}

/**
 * Remove all exception patients as per buisness logic. Removes gold package purchased patients,
 * and coupon code users for OLA and Swiggy.
 * @param  {Array} patientIdList [Array of number consisting of patientId]
 * @return {Array}               [Array of number consisting of patientId]
 */
const removeExceptionPatientId = async (patientIdList) => {
  let mainPatientIdList,
    nonPackagePatientIdList,
    nonCouponCodePatientIdList,
    notRequiredPatientIdList = [],
    nonCorporatePatientIdList = [],
    nonPayLaterPatientIdList = [],
    requiredPatientIdList;

  mainPatientIdList = Array.from(patientIdList);

  patientIdList = await ameyoService.removeThreeDayConnectedCall(patientIdList);
  nonPayLaterPatientIdList = await orderService.removePayLaterConsultationPatient(patientIdList);
  nonPackagePatientIdList = await packageService.removeGoldPackagePatient(nonPayLaterPatientIdList);
  nonCorporatePatientIdList = await packageService.removeCorporatePatient(nonPackagePatientIdList);
  nonCouponCodePatientIdList = await packageService.removeCouponCodePatientList(nonCorporatePatientIdList);

  mainPatientIdList.forEach((patientId) => {
    let isException = nonCouponCodePatientIdList.indexOf(patientId) === -1;

    isException && notRequiredPatientIdList.push(patientId);
  });

  requiredPatientIdList = await patientService.removeAssociatedPatientId(nonCouponCodePatientIdList,
    notRequiredPatientIdList);

  return requiredPatientIdList;
}

/**
 * Calculate total score for list of patientId. Calculate score for patient's age, net payment,
 * consultation count and distinct family members.
 * @param  {Array} patientIdList [Array of number consisting of patientId]
 * @return {Object}              [Object containing patientId mapped patient attribute value and score]
 */
const calculatePatientScore = async (patientIdList) => {
  let age,
    ageScore,
    consultCount,
    consultCountScore,
    payment,
    paymentScore,
    distinctFamily,
    distinctFamilyScore,
    netScore,
    patientPaymentMap,
    patientConsultCountMap,
    patientAgeAndDistinctMap,
    patientScoreMap = {};

  [ patientPaymentMap, patientConsultCountMap, patientAgeAndDistinctMap ] = await Promise.all([
    calculatePatientPayment(patientIdList),
    calculatePatientConsultCount(patientIdList),
    calculatePatientAgeAndDistinctFamily(patientIdList)
  ]);

  patientIdList.forEach((patientId) => {
    age = _.get(patientAgeAndDistinctMap, `${patientId}.age`, 0);
    distinctFamily = _.get(patientAgeAndDistinctMap, `${patientId}.distinctFamilyCount`, 0);
    payment = patientPaymentMap[patientId] || 0;
    consultCount = patientConsultCountMap[patientId] || 0;

    ageScore = scoreCardUtil.getAgeScore(age);
    paymentScore = scoreCardUtil.getPaymentScore(payment);
    consultCountScore = scoreCardUtil.getConsultationCountScore(consultCount);
    distinctFamilyScore = scoreCardUtil.getDistinctFamilyCount(distinctFamily);

    netScore = paymentScore + consultCountScore + ageScore + distinctFamilyScore;

    patientScoreMap[patientId] = {
      patientId,
      score: netScore,
      payment,
      paymentScore,
      age,
      ageScore,
      consultCount,
      consultCountScore,
      distinctFamily,
      distinctFamilyScore
    };
  });

  return patientScoreMap;
}

/**
 * Calculate total payment value for list of patientId. Value is calculated
 * for successfull capture of payment for a consultation
 * @param  {Array} patientIdList [Array of number consisting of patientId]
 * @return {Object}              [Object containing patientId mapped to payment value]
 */
const calculatePatientPayment = async (patientIdList) => {
  const query = {
      attributes: ['patientId'],
      patientId: patientIdList,
      refunded: 0,
      paymentStatus: 'success',
      groupBy: 'patientId',
      $or: [
        { paymentType: { $eq: 'CONSULTATION' } },
        {
          $and: [
            { paymentType: { $eq: null } },
            { consultationId: { $ne: '0'} },
            { consultationId: { $ne: null } }
          ]
        }
      ]
    },
    patientPaymentMap = {};
  let orderDataList,
    patientIdToPaymentMap = {};

  orderDataList = await paymentUtil.fetchPatientNetPayment(query);
  patientIdToPaymentMap = commonHelper.transformListToPropertyMapObject(orderDataList, 'patientId');

  patientIdList.forEach((patientId) => {
    patientPaymentMap[patientId] = _.get(patientIdToPaymentMap, `${patientId}.netPaidAmount`, 0);
  });

  return patientPaymentMap;
}

/**
 * Calculate total consultation count value for list of patientId. Value is calculated
 * for successfull capture of payment for a consultation
 * @param  {Array} patientIdList [Array of number consisting of patientId]
 * @return {Object}              [Object containing patientId mapped to consult count value]
 */
const calculatePatientConsultCount = async (patientIdList) => {
  const query = {
      attributes: ['patientId'],
      patientId: patientIdList,
      refunded: 0,
      paymentStatus: 'success',
      groupBy: 'patientId',
      $or: [
        { paymentType: { $eq: 'CONSULTATION' } },
        {
          $and: [
            { paymentType: { $eq: null } },
            { consultationId: { $ne: '0'} },
            { consultationId: { $ne: null } }
          ]
        }
      ]
    },
    patientConsultCountMap = {};
  let orderDataList,
    patientIdToConsultCountMap = {};

  orderDataList = await paymentUtil.fetchPatientConsultCount(query);
  patientIdToConsultCountMap = commonHelper.transformListToPropertyMapObject(orderDataList, 'patientId');

  patientIdList.forEach((patientId) => {
    patientConsultCountMap[patientId] = _.get(patientIdToConsultCountMap, `${patientId}.consultationCount`, 0);
  });

  return patientConsultCountMap;
}

/**
 * Calculate age score and distinct family count value for list of patientId. Age and family value
 * is calculated upon past paid consultation data.
 * @param  {Array} patientIdList [Array of number consisting of patientId]
 * @return {Object}              [Object containing patientId mapped to age and family value]
 */
const calculatePatientAgeAndDistinctFamily = async (patientIdList) => {
  const query = {
      patientId: patientIdList,
      valid: true,
      ageRange: {
        above: 1,
        below: 100
      }
    },
    patientConsultMapping = {},
    patientScoreMapping = {};
  let consultDataList;

  consultDataList = await consultUtil.fetchConsultsWrapper(query);
  consultDataList.forEach(({ patient }, index) => {
    if (!patientConsultMapping[patient]) {
      patientConsultMapping[patient] = [];
    }

    patientConsultMapping[patient].push(consultDataList[index]);
  });

  Object.keys(patientConsultMapping).forEach((patientId) => {
    let currentConsultList,
      distinctFamilyMemberCount,
      currentPatientAge;

    currentConsultList = patientConsultMapping[patientId];
    distinctFamilyMemberCount = consultUtil.getDistinctFamilyMemberCount(currentConsultList);
    currentPatientAge = consultUtil.getPatientAgeFromConsultData(currentConsultList);

    patientScoreMapping[patientId] = {
      age: currentPatientAge,
      distinctFamilyCount: distinctFamilyMemberCount
    }
  });

  return patientScoreMapping;
}

module.exports = {
  segmentScoreList,
  getCampaignLeadScoreList,
  removeExceptionPatientId,
  calculatePatientScore,
  calculatePatientPayment,
  calculatePatientConsultCount,
  calculatePatientAgeAndDistinctFamily
}