const _ = require('lodash');

const { leadScoreUtil, ameyoUtil, leadAttributeScoreUtil } = require('@utils'),
  { LEAD_SCORE_TABLE_ATTRIBUTES } = require('@constants'),
  { logHelper } = require('@helpers');

const logVernaculatLeadScoreData = async (vernacularLeadScoreData) => {
  let locationList = Object.keys(vernacularLeadScoreData);
  let campaignName;

  for (let location of locationList) {
    campaignName = ameyoUtil.getAmeyoVernacularCampaignKey(location);

    logHelper.log('Location: ', location);

    await Promise.all([
      logLeadScore(vernacularLeadScoreData[location], { campaignName }),
      logAttributeScore(vernacularLeadScoreData[location])
    ]);
  }
}

const logFollowUpLeadScoreList = async (leadScoreList) => {
  let campaignName;

  campaignName = ameyoUtil.getAmeyoDispositionCampaignKey();

  await Promise.all([
    logLeadScore(leadScoreList, { campaignName }),
    logAttributeScore(leadScoreList)
  ]);

  return;
}

const logNotConnectedCallsLeadScoreList = async (leadScoreList) => {
  let campaignName;

  campaignName = ameyoUtil.getAmeyoDispositionNotConnectedCampaignKey();

  await Promise.all([
    logLeadScore(leadScoreList, { campaignName }),
    logAttributeScore(leadScoreList)
  ]);

  return;
}

const logPaidConsultLeadScoreList = async (leadScoreList) => {
  let campaignName;

  campaignName = ameyoUtil.getAmeyoLeadScoreV2CampaignKey();

  await Promise.all([
    logLeadScore(leadScoreList, { campaignName }),
    logAttributeScore(leadScoreList)
  ]);

  return;
}

const logRecentPaidConsultLeadScoreList = async (leadScoreList) => {
  let campaignName;

  campaignName = ameyoUtil.getAmeyoRecentPaidCampaignKey();

  await Promise.all([
    logLeadScore(leadScoreList, { campaignName }),
    logAttributeScore(leadScoreList)
  ]);

  return;
}

const logPastPaidConsultLeadScoreList = async (leadScoreList) => {
  let campaignName;

  campaignName = ameyoUtil.getAmeyoPastPaidCampaignKey();

  await Promise.all([
    logLeadScore(leadScoreList, { campaignName }),
    logAttributeScore(leadScoreList)
  ]);

  return;
}

const logUnPaidConsultLeadScoreList = async (leadScoreList) => {
  let campaignName;

  campaignName = ameyoUtil.getAmeyoUnpaidConsultCampaignKey();

  await Promise.all([
    logLeadScore(leadScoreList, { campaignName }),
    logAttributeScore(leadScoreList)
  ]);

  return;
}

const logShortDurationConnectedLeadScoreList = async (leadScoreList) => {
  let campaignName;

  campaignName = ameyoUtil.getAmeyoShortDurationConnectedCampaignKey();

  await Promise.all([
    logLeadScore(leadScoreList, { campaignName }),
    logAttributeScore(leadScoreList)
  ]);

  return;
}

const logFailedPaymentLeadScoreList = async (leadScoreList) => {
  let campaignName;

  campaignName = ameyoUtil.getAmeyoFailedPaymentCampaignKey();

  await Promise.all([
    logLeadScore(leadScoreList, { campaignName }),
    logAttributeScore(leadScoreList)
  ]);

  return;
}

const logAttributeScore = async (leadAttributeScore, params) => {
  let writeDataList = [],
    metaObject = {},
    writeObject = {};

  leadAttributeScore.forEach((listElement) => {
    Object.keys(listElement).forEach((listElementProperty) => {
      if (!listElement[`${listElementProperty}Score`]) {
        return;
      }

      metaObject = {
        patientId: listElement.patientId,
        attribute: listElementProperty,
        value: listElement[listElementProperty],
        score: listElement[`${listElementProperty}Score`]
      };

      writeObject = {
        ...metaObject,
        meta: JSON.stringify(metaObject)
      }

      writeDataList.push(writeObject);
    });
  });

  await leadAttributeScoreUtil.writeScoreToDB(writeDataList);

  return;
}

/**
 * Log patient Lead score into LeadScore table.
 * @param  {[Array, Object]} leadScore [ Patient leads score details.Object specifies mapping of
 *                                       PatientId to its corresponding score. List/Array specifies elements
 *                                       having attributes similar to LeadsScore table column (patientId,
 *                                       score, version, campaignName, etc)]
 * @param  {Object} params             [ Attributes similar to LeadScore table column ]
 */
const logLeadScore = async (leadScore, params = {}) => {
  let writeObject = {},
    writeDataList = [];

  if (typeof leadScore === 'object') {
    if (!Array.isArray(leadScore)) {
      Object.keys(leadScore).forEach((patientId) => {
        writeObject = {
          patientId,
          score: leadScore[patientId],
          ...params
        }

        writeDataList.push(leadScoreUtil.getLeadScoreWriteObject(writeObject));
      });
    } else {
      leadScore.forEach((leadScoreElement) => {
        writeObject = {
          ..._.pick(leadScoreElement, LEAD_SCORE_TABLE_ATTRIBUTES),
          ..._.pick(params, LEAD_SCORE_TABLE_ATTRIBUTES)
        }

        writeDataList.push(leadScoreUtil.getLeadScoreWriteObject(writeObject));
      });
    }
  }

  logHelper.log(`Logging Campaign Params: ${JSON.stringify(params)}`);
  logHelper.log(`Logging Element's Count: ${writeDataList.length}`);
  logHelper.log(`Logging Element's Data: ${JSON.stringify(writeDataList)}`);

  return await leadScoreUtil.writeScoreToDB(writeDataList);
}

module.exports = {
  logAttributeScore,
  logLeadScore,
  logFollowUpLeadScoreList,
  logFailedPaymentLeadScoreList,
  logNotConnectedCallsLeadScoreList,
  logPaidConsultLeadScoreList,
  logShortDurationConnectedLeadScoreList,
  logVernaculatLeadScoreData,
  logUnPaidConsultLeadScoreList,
  logRecentPaidConsultLeadScoreList,
  logPastPaidConsultLeadScoreList
}