const fs = require('fs');

fs.readdirSync(__dirname + '/').forEach((file) => {
  let name;

  if (file.match(/\.js$/) !== null && file !== 'index.js' && file !== 'internal.js') {
    name = file.replace('.js', '');
    exports[name] = require('./' + file);
  }
});