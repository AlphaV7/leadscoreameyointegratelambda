##LeadScoreAmeyoIntegrate Lambda ChangeLog

####1.3.4 - 15 April 2019
* Remove patientId existing in Subscription

####1.3.3 - 09 February 2019
* Optimise query to fetch follow-up leads (v3)

####1.3.2 - 04 February 2019
* Optimise query to fetch follow-up leads

####1.3.1 - 31 January 2019
* Remove pay-later consultation patients

####1.3.0 - 17 January 2019
* Automate vernacular experiment for Andhra region

####1.2.9 - 18 December 2018
* Add short duration connected leads to Ameyo campaign.

####1.2.8 - 18 December 2018
* Migrate scoring logic to scorecard V2.
* Log Campaign Data into Database table: LeadScore.

####1.2.7 - 10 December 2018
* Add function to remove corporate users.

####1.2.6
* Modify Disposition campaign list to fetching leads for followup campaign
* Bugfix for invalid phonenumber format corresponding to DB fetch
* Add attempted but not connected calls to Ameyo

####1.2.5
* Configure logging for Lambda

####1.2.4
* Remove not paid gold coupon users prior to ameyo lambda call

####1.2.3
* Remove duplicate phonenumber prior to ameyo lambda call

####1.2.2
* Remove coupon patientId associated with common coupon user

####1.2.1
* Remove Swiggy and OLA coupon users

####1.2.0
* Modify configuration to handle multiple campagin based on threshold score
* Multiple campaign support only on summarized and unpaid consultation
* Fix bug for posting recent disposition patient details

####1.1.0
* Fetch disposition data for lead score campaign from past 8th, 10th and 12th day. ( A )
* Fetch past week bad NPS patient. ( B )
* Add separate campaign for disposition lead.
* Filter non gold users, remove patients which exist in ( B ) and push leads to ameyo SQS.

####1.0.0
* Fetch summarized consult patient list for set interval and exclude consults with bad nps.( A )
* Fetch patient list for patient's above threshold updated within set interval. ( B )
* Fetch patient list for patient's with unpaid consultaion within set interval. ( C )
* Filter out non-package users from (A U B U C) and push leads to ameyo