const Promise = require('bluebird');

const promiseDelay = async (duration) => {
  await Promise.delay(duration || 0);
  return;
}

module.exports = {
  promiseDelay
}