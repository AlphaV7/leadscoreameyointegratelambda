const _ = require('lodash');

const generateAttributeQueryObject = (attributes) => {
  if (typeof attributes === 'string') {
    return [attributes];
  }

  if (Array.isArray(attributes)) {
    return attributes;
  }

  return;
}

const generateRangeQueryObject = (rangeList) => {
  const query = {
      $or: []
    },
    currentRangeList = (rangeList && Array.isArray(rangeList)) ? rangeList : [rangeList];
  let currentRange = {};

  (currentRangeList || []).forEach((range) => {
    currentRange = {};

    range.above && _.set(currentRange, `$gte`, range.above);
    range.below && _.set(currentRange, `$lte`, range.below);
    query.$or.push(currentRange);
  });

  return query;
}

const generateEqualQueryObject = (data) => {
  const query = {};

  Array.isArray(data) && data.length && _.set(query, '$in', data);
  (typeof data !== 'object' || data === null) && _.set(query, '$eq', data);

  if (_.isEmpty(query)) {
    return ' ';
  }

  return query;
}

const generateLikeQueryObject = (data) => {
  const query = {},
    likeOperator = '$like';

  if (Array.isArray(data)) {
    query.$or = [];

    data.forEach((element) => {
      query.$or.push(_.set({}, `${likeOperator}`, element));
    });
  }

  typeof data !== 'object' && _.set(query, `${likeOperator}`, data);

  if (_.isEmpty(query)) {
    return ' ';
  }

  return query;
}

const generateNotLikeQueryObject = (data) => {
  const query = {},
    notLikeOperator = '$notLike';

  Array.isArray(data) && data.length && _.set(query, `${notLikeOperator}.$any`, data);
  typeof data !== 'object' && _.set(query, `${notLikeOperator}`, data);

  if (_.isEmpty(query)) {
    return ' ';
  }

  return query;
}

const generateNotEqualQueryObject = (data) => {
  const query = {};

  Array.isArray(data) && data.length && _.set(query, '$notIn', data);
  (typeof data !== 'object' || data === null) && _.set(query, '$ne', data);

  if (_.isEmpty(query)) {
    return ' ';
  }

  return query;
}

const generateQueryOrderByObject = (orderList) => {
  const query = [],
    currentOrderList = (orderList && Array.isArray(orderList)) ? orderList : [orderList];
  let currentData = [];

  currentOrderList.forEach((currentOrder) => {
    currentData = [];

    typeof currentOrder.column === 'string' && currentData.push(currentOrder.column);
    (currentOrder.asc === true || currentOrder.desc === false) && currentData.push('ASC');
    (currentOrder.asc === false || currentOrder.desc === true) && currentData.push('DESC');

    currentData.length === 2 && query.push(currentData);
  });

  return query;
}

module.exports = {
  generateNotLikeQueryObject,
  generateAttributeQueryObject,
  generateLikeQueryObject,
  generateQueryOrderByObject,
  generateEqualQueryObject,
  generateRangeQueryObject,
  generateNotEqualQueryObject
}