const _ = require('lodash'),
  Promise = require('bluebird'),
  xml2js = require('xml2js');

const { logHelper } = require('@helpers'),
  bugsnag = require('@bugsnag');

const calculateListMode = (array) => {
  const modeMap = {};
  let maxEl,
    maxCount;

  if (!array.length) {
    return null;
  }

  maxEl = array[0];
  maxCount = 1;

  for (let index = 0; index < array.length; index++) {
    var el = array[index];

    if (!modeMap[el]) {
      modeMap[el] = 1;
    }
    else {
      modeMap[el]++;
    }

    if (modeMap[el] > maxCount){
      maxEl = el;
      maxCount = modeMap[el];
    }
  }

  return maxEl;
}

const filterByCount = (mainCollection, filterData, minCount = 0, maxCount = Number.MAX_SAFE_INTEGER) => {
  const requiredCollection = [],
    elementCountMapping = {};
  let currentFilterValueList,
    currentValue,
    currentCount,
    currentStringifiedId,
    currentElementList,
    isValidElement;

  mainCollection.forEach((mainCollectionElement) => {
    isValidElement = true;

    Object.keys(filterData).forEach((currentProperty) => {
      currentFilterValueList = Array.isArray(filterData[currentProperty]) ? filterData[currentProperty] :
        [filterData[currentProperty]];
      currentValue = _.get(mainCollectionElement, `${currentProperty}`);

      if (!currentFilterValueList || !currentValue || currentFilterValueList.indexOf(currentValue) === -1) {
        isValidElement = false;
      }

      isValidElement = isValidElement && currentFilterValueList.indexOf(currentValue) !== -1;
    });

    currentStringifiedId = JSON.stringify(_.pick(mainCollectionElement, Object.keys(filterData)));

    currentCount = _.get(elementCountMapping, `${currentStringifiedId}.count`, 0);
    currentElementList = _.get(elementCountMapping, `${currentStringifiedId}.element`, []);

    if (!isValidElement) {
      return;
    }

    currentElementList.push(mainCollectionElement);

    _.set(elementCountMapping, `${currentStringifiedId}`, {
      count: currentCount + 1,
      element: currentElementList
    });
  });

  Object.keys(elementCountMapping).forEach((collectionElement) => {
    currentCount = _.get(elementCountMapping, `${collectionElement}.count`, -1);
    currentElementList = _.get(elementCountMapping, `${collectionElement}.element`, []);

    if (currentCount >= minCount && currentCount <= maxCount) {
      requiredCollection.push(...currentElementList);
    }
  });

  return requiredCollection;
}

const transformObjectToNumberList = (ObjectList, property) => {
  const transformedList = [],
    initialList = Array.isArray(ObjectList) ? ObjectList : [ObjectList];

  initialList.forEach((object) => {
    let element = _.get(object, property);

    if (!element) {
      return;
    }

    try {
      element && !_.isNaN(parseInt(element)) && transformedList.push(parseInt(element));
    } catch (err) {
      logHelper.log('Unable to convert data to number format. Data: ', object);
      bugsnag.notify(err);
    }

  });

  return transformedList;
}

const transformObjectToStringList = (ObjectList, property) => {
  const transformedList = [],
    initialList = Array.isArray(ObjectList) ? ObjectList : [ObjectList];

  initialList.forEach((object) => {
    let element = _.get(object, property);

    element && transformedList.push(String(element));
  });

  return transformedList;
}

const transformListToPropertyMapObject = (ObjectList, property, uniqueThroughList = true) => {
  const transformedObject = {},
    initialList = Array.isArray(ObjectList) ? ObjectList : [ObjectList];

  let currentPropertyData;

  initialList.forEach((object) => {
    currentPropertyData = _.get(object, `${property}`);

    if (!currentPropertyData || Array.isArray(currentPropertyData) || typeof currentPropertyData === 'object') {
      return;
    }

    if (!uniqueThroughList) {
      if (!_.get(transformedObject, `${currentPropertyData}`)) {
        transformedObject[currentPropertyData] = [];
      }

      transformedObject[currentPropertyData].push(object);
    } else {
      _.set(transformedObject, `${currentPropertyData}`, object);
    }
  });

  return transformedObject;
}

const generateABTestSetByEvenOddProperty = (ObjectList, property) => {
  const testSet = { setA: [], setB: [] },
    initialList = Array.isArray(ObjectList) ? ObjectList : [ObjectList];
  let currentPropertyData;

  if (!initialList || !initialList.length) {
    return testSet;
  }

  initialList.forEach((listElement) => {
    currentPropertyData = property ? _.get(listElement, `${property}`) : listElement;
    currentPropertyData = parseInt(currentPropertyData);

    currentPropertyData%2 === 0 && testSet.setA.push(listElement);
    currentPropertyData%2 === 1 && testSet.setB.push(listElement);
  });

  return testSet;
}

const parseXmlString = async (xmlString) => {
  return new Promise((resolve, reject) => {
    xml2js.parseString(xmlString, (err, result) => {
      return resolve(result);
    });
  });
}

module.exports = {
  calculateListMode,
  filterByCount,
  parseXmlString,
  transformObjectToNumberList,
  transformObjectToStringList,
  generateABTestSetByEvenOddProperty,
  transformListToPropertyMapObject
}