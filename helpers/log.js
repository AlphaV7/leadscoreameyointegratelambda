const DEBUG = true;

const log = function () {
  if (DEBUG){
    console.log.apply(console, arguments);
  }
}

module.exports = {
  log
}