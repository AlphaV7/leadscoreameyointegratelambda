# Lead score lambda to push leads to ameyo sqs

LeadScoreAmeyoIntegratge is an aws lambda function for processing patient's net lead score data and pushing adequate leads to ameyo. Currently lambda set to trigger every 24 hours at 1:00:00 UTC.

  - Lambda link: https://us-west-2.console.aws.amazon.com/lambda/home?region=us-west-2#/functions/leadScoreAmeyoIntegrateLambda

# Process:

  - Fetch summarized consult patient list for set interval and exclude consults with bad nps.( A )
  - Fetch patient list for patient's with unpaid consultaion within set interval. ( B )
  - Filter out patient in ( B ) which are above threshold
  - Filter out non-package users from (A U B U C) and push leads to ameyo SQS (campaignId: 16)

  - Fetch disposition data for lead score campaign from past 8th, 10th and 12th day. ( A )
  - Remove patientIds with bad NPS from past week. ( B )
  - Filter out non-package users from and push leads to ameyo SQS (campaignId: 7)

### Tech

Lambda uses the following to work properly:

* [node.js]

### Installation

Lambda requires [Node.js](https://nodejs.org/) v8+ to run.

Install the dependencies and devDependencies and start the server.

```sh
$ cd leadScoreAmeyoIntegrate
$ npm install -d
$ node index.js
```

NOTE: Currently not set to act upon NODE_ENV. Manually run index.handler to run lambda.

### Todos

 - Write Tests

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen.)

   [node.js]: <http://nodejs.org>
